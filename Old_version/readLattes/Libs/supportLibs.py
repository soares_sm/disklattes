# -*- coding: utf-8 -*-
"""
####################################################################################################
# @title: nome do módulo
# @Description: Modulo do programa readLattes para ...
#				
#
# @Author: Solon Macedonia Soares
# @Email: soares.solonm@gmail.com
# 
# @Version: 2.0
# @Data_release: xx/xx/20xx
####################################################################################################
"""
########################### PATHS PROGRAM ###########################
# PATHS BASE
pathMain = 'C:\\readLattes\\'										# Endereço raiz do programa
pathDatabase = pathMain + 'Database\\'								# Endereço da base de dados
pathExport = pathMain + 'Exporta\\'									# Path para exportação de dados
pathLibs = pathMain + 'Libs\\'										# Endereço da biblioteca de modulos
pathLog = pathMain + 'Logs\\'										# Path Logs do sistema

# SUBPATHS BASE
subPathCVlattes = pathDatabase + 'CVLattes\\'						# Endereço curriculos Lattes
subPathZips = pathDatabase + 'Zips\\'								# Endereço arquivos Zips
subPathExport = pathExport + 'iGraph\\'								# Endereço resultados do iGraph
subPathMatriz = pathExport + 'MatrizAdjacencia\\'					# Endereço matriz de adjacencia do Grafo
subPathProducao = pathExport + 'ProducaoPPG\\'						# Endereço análise da produção do programa

########################## IMPORTS MODULOS ##########################
# Oficial modules
																
# Support readLattes modules
import supportDatabase		as modDatabase							# Modulo para leitura da base de dados
import supportExport		as modExport							# Modulo de exportação de dados
import supportLattes		as modLattes							# Módulo de suporte as atividades Lattes
import supportMatriz		as modMatriz							# Módulo de suporte as atividades com matrizes
import supportLog			as modLog								# Módulo de Log do programa
import supportPPG			as modPPG								# Módulo de análise da estrutura dos programas
import supportValidation	as modValid								# Módulo de Validação de dados
import supportXML			as modXML								# Módulo de manuseio de XML

############################### FILES ###############################
# FILES
logFileRun = 'LOG_RUN_PROGRAM_'										# Log de execução do progama
filePPG = pathMain + 'PPGs.txt'										# Arquivo com a lista os programas
fileNotasPPG = pathDatabase + 'ppgNotas.csv'						# Arquivo com a lista de nota separada por ano dos programas
fileListId = pathDatabase + 'anoDocencia.csv'						# Arquivo com a lista de Lattes IDs
fileListPPG = pathDatabase + 'listaDadosPPG.csv'					# Arquivo com a lista de dados do PPGs

############################# VARIABLES #############################
dicHeaderPPG = ['ppg','sigla','adm','modal','grau','regiao',
			'uf','area','subarea','avaliacao']						# Cabeçario do arquivo listaDadosPPG.csv (ref: (0,1,2,3,4,5,6,7,8,9)
dicHeaderTeacher = ['ano','ppg','id','sexo','xp']					# Cabeçario do arquivo anoDocencia.csv (ref: (0,1,2,3,4)
listStruturaType = ['regiao','uf','adm']							# 
listManagementType = ['PARTICULAR','FEDERAL','ESTADUAL']

######################### FUNCTIONS PROGRAM #########################
