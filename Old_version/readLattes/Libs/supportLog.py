# -*- coding: utf-8 -*-
"""
####################################################################################################
# @title: nome do módulo
# @Description: Modulo do programa readLattes para ...
#				
#
# @Author: Solon Macedonia Soares
# @Email: soares.solonm@gmail.com
# 
# @Version: 2.0
# @Data_release: xx/xx/20xx
####################################################################################################
"""
######################### PATHS PROGRAM #########################
# PATHS BASE
pathMain = 'C:\\readLattes\\'									# Endereço raiz do programa
pathLibs = pathMain + 'Libs\\'									# Endereço da biblioteca de modulos

######################## IMPORTS MODULOS ########################
# Oficial modules
from datetime import datetime									# Módulo para usar Datatime

# Support readLattes modules
import supportLibs		as modLibs								# Modulo de acesso a base de dados

# Analises modules

############################# FILES #############################
# FILES
logFileRun = "";												# Log de execução do progama

####################### FUNCTIONS PROGRAM #######################
# Definição da função aqui										# Função
def printModulo():
	print('supportLog')

	return 0

# Datetime to file name											# Função
def fun_DateFileLog():
	dateNow = datetime.now();
	dateToFile = dateNow.strftime('%Y%m%d_%H%M%S')
	
	return dateToFile

# Datetime used with data in the log							# Função
def fun_DatetimeDefined():
	dateNow = datetime.now()
	dateNow = dateNow.strftime('%d/%m/%Y %H:%M:%S')
	
	return dateNow
	
# Cria arquivo de Log
def fun_CreateLogFile(logFile):
	msgLog = ("##### Inicio: " + fun_DatetimeDefined() + " #####")
	logFile = modLibs.pathLog + logFile + fun_DateFileLog() + '.txt'
	dataLog = open(logFile, "a+")
	dataLog.write(msgLog)
	dataLog.close()
	print(msgLog)
	
	return logFile

# Create LOG File	
def fun_AddLog(logFile,msgLog):
	fLog = open(logFile, "a+")
	fLog.write("\n")
	fLog.write(msgLog)
	fLog.close()
	
	return 0

# Finish LOG File
def fun_FinishLogFile(logFile):
	msgLog = ("\n##### Termino: " + fun_DatetimeDefined() + " #####");
	fun_AddLog(logFile,msgLog);
	print(msgLog)
	
	print("\nArquivo de LOG: " + logFile)
	exit()
	
	return 0