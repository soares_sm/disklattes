# -*- coding: utf-8 -*-
"""
####################################################################################################
# @title: nome do módulo
# @Description: Modulo do programa readLattes para análises da estrutura do programa ...
#				
#
# @Author: Solon Macedonia Soares
# @Email: soares.solonm@gmail.com
# 
# @Version: 2.0
# @Data_release: xx/xx/20xx
####################################################################################################
"""
########################### PATHS PROGRAM ###########################
# PATHS BASE
pathMain = 'C:\\readLattes\\'										# Endereço raiz do programa
pathLibs = pathMain + 'Libs\\'										# Endereço da biblioteca de modulos

########################## IMPORTS MODULOS ##########################
# Oficial modules
																
# Support readLattes modules
import supportDatabase		as modDatabase							# Modulo para leitura da base de dados
import supportExport		as modExport							# Modulo de exportação de dados
import supportMatriz		as modMatriz							# Módulo de suporte as atividades com matrizes
import supportLibs			as modLibs								# Modulo de acesso a base de dados
import supportLog			as modLog								# Módulo de Log do programa

######################### FUNCTIONS PROGRAM #########################
# Definição da função aqui
def printModulo():
	print('supportExport')

	return 0

# Análise da estrutura do(s) programa(s)
def fun_StruturePPG(listYear,fileLog,listRegions,findFilter,struturetype):
	# lista as opções de WHERE, Filtros e Header nos arquivos
	listWhere = ['ACADEMICO','PROFISSIONAL','INTERDISCIPLINAR',
				'ENGENHARIA/TECNOLOGIA/GESTAO','45']				# Clausula Where do arquivo listaDadosPPG.csv (ref: (3,3,7,8,9))
	listFilter = [findFilter,'MASCULINO','FEMININO']				# Filtro para os arquivos listaDadosPPG (ref: 5) e anoDocencia.csv (ref: 3)
	dicHeaderPPG = modLibs.dicHeaderPPG								# Cabeçario do arquivo listaDadosPPG.csv
	dicHeaderTeacher = modLibs.dicHeaderTeacher						# Cabeçario do arquivo anoDocencia.csv
	
	"""
	# A analise está fechada para esta versão, entendo ter 
	#	material para criar a possibilidade de abrir a pesquis
	#	a partir de filtros pré estabelecidos.
	#
	# 1. Area: Interdisciplinar
	# 2. Subarea: Engenharia, Tecnologia e Gestão
	# 3. Modalidade: Separado em duas (Acadêmico e Profissional)
	# 4. Observado pore Regiao e por Estado	
	"""		
	# Identifica estrutura ACADEMICO
	msgLog = '	> Observacao pela modalidade %s #####' % listWhere[0]
	modLog.fun_AddLog(fileLog,msgLog)
	print(msgLog)
	
	nameToFile = listWhere[0] + '_' + listWhere[2] + '_' + listWhere[3] + '_' + modLog.fun_DateFileLog()
	nameToFile = nameToFile.replace('/','-')
	fileCSV = modLibs.subPathProducao + struturetype.upper() + '_' + nameToFile + '.csv'
	writeFileCSV = open(fileCSV, "w")
	writeFileCSV.close()
	
	whereSelecte = [listWhere[0],listWhere[4],listWhere[2],listWhere[3]]
	fun_getStructurePPG(listRegions,listYear,fileLog,whereSelecte,dicHeaderPPG,listFilter,dicHeaderTeacher,fileCSV,findFilter,struturetype)
	
	# Identifica estrutura PROFISSIONAL
	msgLog = '	> Observacao pela modalidade %s #####' % listWhere[1]
	modLog.fun_AddLog(fileLog,msgLog)
	print(msgLog)
		
	nameToFile = listWhere[1] + '_' + listWhere[2] + '_' + listWhere[3] + '_' + modLog.fun_DateFileLog()
	nameToFile = nameToFile.replace('/','-')
	fileCSV = modLibs.subPathProducao + struturetype.upper() + '_' + nameToFile + '.csv'
	writeFileCSV = open(fileCSV, "w")
	writeFileCSV.close()
	
	whereSelecte = [listWhere[1],listWhere[4],listWhere[2],listWhere[3]]
	fun_getStructurePPG(listRegions,listYear,fileLog,whereSelecte,dicHeaderPPG,listFilter,dicHeaderTeacher,fileCSV,findFilter,struturetype)
	
	return 0
	
def fun_getListRegion(nameRegion,whereSelecte,dicHeader,struturetype):
	listRegionPPG = []
	del listRegionPPG[:]

	fileRead = modDatabase.fun_OpenFile(modLibs.fileListPPG)
	for dataLine in fileRead:
		# Verifica os Where Modal
		if whereSelecte[0] == dataLine[dicHeader[3]]:
			# Verifica os Where da forma de Avaliação dos programas
			if  whereSelecte[1] == dataLine[dicHeader[9]]:
				# Verifica os Where da área e da subárea
				if whereSelecte[2] == dataLine[dicHeader[7]] and whereSelecte[3] == dataLine[dicHeader[8]]:
					# Filtra pela região ou estado
					if nameRegion in dataLine[struturetype]:
						listRegionPPG.append(dataLine[dicHeader[0]])
		
	return listRegionPPG

def fun_getData(year,dicHeaderTeacher,whereClausule,listPPG):
	listDataFun = []
	del listDataFun[:]

	xpSum = 0
	for idPPG in listPPG:
		fileRead = modDatabase.fun_OpenFile(modLibs.fileListId)
		for dataLine in fileRead:
			#
			if year == dataLine[dicHeaderTeacher[0]] and idPPG == dataLine[dicHeaderTeacher[1]]:
				if whereClausule == dataLine[dicHeaderTeacher[3]]:
					listDataFun.append(dataLine[dicHeaderTeacher[2]])
					xpSum = xpSum + int(dataLine[dicHeaderTeacher[4]])
			# end if
		#end for
	#end for
	qtdData = len(listDataFun)
	return qtdData,xpSum
	
def fun_getStructurePPG(listRegions,listYear,fileLog,whereSelecte,dicHeaderPPG,listFilter,dicHeaderTeacher,fileCSV,header,struturetype):
	contador = 0
	vectorData = [''] * 20
	
	######################### FUNCTIONS PROGRAM #########################
	headerRegiao = ['ano',header,'modal','ppgs','docente','masculino',
					'feminino','media docente','media masculino',
					'media feminino','% masculino','% feminino',
					'docentes xp','masculino xp','feminino xp','media xp',
					'media xp masculino','media xp feminino',
					'% masculino xp','% feminino xp',]
	modExport.exportLineCSV(fileCSV,headerRegiao)
	
	# Monta dados a partir da região 
	for nameRegion in listRegions:
		msgLog = '		> ##### ' + struturetype.upper() + '_' + nameRegion
		modLog.fun_AddLog(fileLog,msgLog)
		#print(msgLog)
		
		for runListYear in listYear:
			runYear = runListYear.strip()
						
			msgLog = '\n######################### '
			msgLog = msgLog + 'Ano observado: ' + runYear
			modLog.fun_AddLog(fileLog,msgLog)
			
			#####
			# Obtem lista de programas a partir do filtro desejado.
			vectorData[0] = runYear
			vectorData[1] = nameRegion
			vectorData[2] = whereSelecte[0]
					
			listPPG = fun_getListRegion(nameRegion,whereSelecte,dicHeaderPPG,struturetype)
			
			# Pega total de Homens e Anos de experiência masculino
			whereClausule = listFilter[1]
			returnFun = fun_getData(runYear,dicHeaderTeacher,whereClausule,listPPG)
			qtdMasc = float(returnFun[0])
			qtdXPM = float(returnFun[1])
			# Pega total de Homens e Anos de experiência feminino
			whereClausule = listFilter[2]
			returnFun = fun_getData(runYear,dicHeaderTeacher,whereClausule,listPPG)
			qtdFem = float(returnFun[0])
			qtdXPF = float(returnFun[1])
			
			# consolidação de totais
			totalSexo = qtdMasc + qtdFem
			
			
			
			
			if totalSexo > 0:
				totalXP = qtdXPM + qtdXPF
				percentDM = (qtdMasc * 100) / totalSexo
				percentDF = (qtdFem * 100) / totalSexo
				percentXPM = (qtdMasc * 100) / totalSexo
				percentXPF = (qtdFem * 100) / totalSexo
				
				if qtdMasc > 0:
					mediaXPM = qtdXPM / qtdMasc
				else:
					mediaXPM = 0.0
				if qtdFem > 0:
					mediaXPF = qtdXPF / qtdFem
				else:
					mediaXPF = 0.0				
				
				vectorData[3] = '%i' %(len(listPPG))
				vectorData[4] = '%i' %(totalSexo)
				vectorData[5] = '%i' %(qtdMasc)
				vectorData[6] = '%i' %(qtdFem)
				vectorData[7] = '%0.2f' %((totalSexo / len(listPPG)))
				vectorData[8] = '%0.2f' %((qtdMasc / len(listPPG)))
				vectorData[9] = '%0.2f' %((qtdFem / len(listPPG)))
				vectorData[10] = '%0.4f' %(percentDM)
				vectorData[11] = '%0.4f' %(percentDF)
				vectorData[12] = '%i' %(totalXP)
				vectorData[13] = '%i' %(qtdXPM)
				vectorData[14] = '%i' %(qtdXPF)
				vectorData[15] = '%0.2f' %((totalXP / totalSexo))
				vectorData[16] = '%0.2f' %(mediaXPM)
				vectorData[17] = '%0.2f' %(mediaXPF)
				vectorData[18] = '%0.4f' %(percentXPM)
				vectorData[19] = '%0.4f' %(percentXPF)
				
				modExport.exportLineCSV(fileCSV,vectorData)
			else:
				vectorData[2] = '%i' %(0)
				vectorData[3] = '%i' %(0)
				vectorData[4] = '%i' %(0)
				vectorData[5] = '%i' %(0)
				vectorData[6] = '%0.2f' %(0)
				vectorData[7] = '%0.2f' %(0)
				vectorData[8] = '%0.2f' %(0)
				vectorData[9] = '%0.4f' %(0)
				vectorData[10] = '%0.4f' %(0)
				vectorData[11] = '%i' %(0)
				vectorData[12] = '%i' %(0)
				vectorData[13] = '%i' %(0)
				vectorData[14] = '%0.2f' %(0)
				vectorData[15] = '%0.2f' %(0)
				vectorData[16] = '%0.2f' %(0)
				vectorData[17] = '%0.4f' %(0)
				vectorData[18] = '%0.4f' %(0)
				vectorData[19] = '%0.4f' %(0)
			
			"""
			# apresentação dos dados para checar
			print('\nAnalise Geral')
			print('')
			print('Total de programas: %i' %(len(listPPG)))
			print('Total de docentes: %i' %totalSexo)
			print('Total de docentes masculino: %i' %qtdMasc)
			print('Total de docentes feminino: %i' %qtdFem)
			print('Media de docentes: %0.2f' %(totalSexo / len(listPPG)))
			print('Media de docentes masculino: %0.2f' %(qtdMasc / len(listPPG)))
			print('Media de docentes feminino: %0.2f' %(qtdFem / len(listPPG)))
			print('Porcentagem masculino: %0.4f' %percentDM)
			print('Porcentagem feminino: %0.4f' %percentDF)
			print('')
			print('Total de experiencia dos docentes: %i' %totalXP)
			print('Total de experiencia dos docentes masculino: %i' %qtdXPM)
			print('Total de experiencia dos docentes feminino: %i' %qtdXPF)
			print('Media de experiencia dos docentes: %0.2f' %(totalXP / totalSexo))
			print('Media de experiencia dos docentes masculino: %0.2f' %mediaXPM)
			print('Media de experiencia dos docentes feminino: %0.2f' %mediaXPF)
			print('Porcentagem de experiencia masculino: %0.4f' %percentXPM)
			print('Porcentagem de experiencia feminino: %0.4f' %percentXPF)
			"""	
			
		#end for
	#print(matrizRegions)
	#end for
	return 0