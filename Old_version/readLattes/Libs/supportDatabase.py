# -*- coding: utf-8 -*-
"""
####################################################################################################
# @title: nome do módulo
# @Description: Modulo do programa readLattes para ...
#				
#
# @Author: Solon Macedonia Soares
# @Email: soares.solonm@gmail.com
# 
# @Version: 2.0
# @Data_release: xx/xx/20xx
####################################################################################################
"""
######################### PATHS PROGRAM #########################
# PATHS BASE
pathMain = 'C:\\readLattes\\'									# Endereço raiz do programa
pathLibs = pathMain + 'Libs\\'									# Endereço da biblioteca de modulos

######################## IMPORTS MODULOS ########################
# Oficial modules
import os														# 
import csv														# 
													
# Support readLattes modules
import supportLibs			as modLibs							# Modulo de acesso a base de dados
import supportLog			as modLog							# Módulo de Log do programa

# Analises modules

####################### FUNCTIONS PROGRAM #######################
# Definição da função aqui
def printModulo():
	print('supportDatabase')

	return 0

# Verifica se o arquivo Existe
def fun_CheckFile(fileToCheck):
	fileCheck = False;
	if os.path.isfile(fileToCheck):
		fileCheck = True;
	
	return fileCheck

# Abre arquivo CSV
def fun_OpenFile(fileName):
	fileRead = csv.DictReader(open(fileName, 'rb'), delimiter=';', quoting=csv.QUOTE_NONE)
	return fileRead
	
# Lista os programas que serão observados
def fun_DataPPG(fileCheck,fileLog):
	checkData = True
	existFile = fun_CheckFile(fileCheck)
	
	if existFile <> checkData:
		msgLog = 'Problema com o arquivo > ' + fileCheck
		modLog.fun_AddLog(fileLog,msgLog)
		print(msgLog)
		modLog.fun_FinishLogFile(fileLog)
	else:
		msgLog = 'Arquivo encontrado: ' + fileCheck
		modLog.fun_AddLog(fileLog,msgLog)
	
	return 0

# Verifica se os dados dos PPGs solicitados estão disponíveis
def fun_DatabasePPG(filePPG,fileListId,fileListPPG,fileLog,pathXML,pathZip):
	listPPG = []
	listId = []
	listPPGRun = []
	
	del listPPG[:]
	del listPPGRun[:]
	
	# Lista programas solicitados
	listPPG = fun_listPPGs(filePPG)
	
	msgLog = 'Quantidade de programa(s) solicitado(s): %0i' %len(listPPG)
	modLog.fun_AddLog(fileLog,msgLog)
	print(msgLog)
	
	# Se não encongtrar PPGs solicitados ele sai do programa
	if len(listPPG) <= 0:
		fun_emptyListPPG(fileLog)
	# end if
	
	# Verifica se os curriculos Lattes estão disponíveis
	programId = ""
	for programId in listPPG:
		del listId[:]
		listIds = fun_listLattesID(fileListId,programId)
	
		msgLog = '> PPG: ' + programId
		modLog.fun_AddLog(fileLog,msgLog)
		#print(msgLog)
		
		msgLog = '	> Lattes Ids: %0i' %len(listIds)
		modLog.fun_AddLog(fileLog,msgLog)
		#print(msgLog)
				
		checkXML = fun_checkLattesFiles(programId,listIds,pathXML,pathZip,fileLog) 
		
		#verfica dados do programa
		if checkXML == True:
			checkPPG = fun_checkDataPPG(fileListPPG,programId)
			if checkPPG == True:
				listPPGRun.append(programId)
			else:		
				msgLog = '\n	=> Dados incompletos do programa %s.' %programId
				modLog.fun_AddLog(fileLog,msgLog)
				print(msgLog)
			# end if
		# end if
	# end for
	
	if len(listPPGRun) <= 0:
		fun_emptyListPPG(fileLog)
	# end if
	
	return listPPGRun

def fun_listPPGs(filePPG):
	listPPG = []
	del listPPG[:]
	
	with open(filePPG) as readFilePPG:
		for listCheckPPGs in readFilePPG:
			programId = listCheckPPGs.rstrip()
			listPPG.append(programId)
		# end for
	# end with
	
	return listPPG

def fun_listLattesID(fileListId,programId):
	listIds = []
	del listIds[:]
	
	fileRead = fun_OpenFile(fileListId)
	for listData in fileRead:
		if programId == listData['ppg']:
			listIds.append(listData['id'])
	
	listIds = list(set(listIds))
	listIds.sort()
	return listIds
	
	return 0
	
def fun_emptyListPPG(fileLog):
	msgLog = '	=> Sem programas para observar, fim do programa.'
	modLog.fun_AddLog(fileLog,msgLog)
	print(msgLog)
	modLog.fun_FinishLogFile(fileLog)

	return 0
	
def fun_checkDataPPG(fileListPPG,programId):
	checkPPG = False
	
	fileRead = fun_OpenFile(fileListPPG)
	for listData in fileRead:
		if programId == listData['ppg']:
			checkPPG = True
	
	return checkPPG

def fun_checkLattesFiles(programId,listIds,pathXML,pathZip,fileLog):
	returnData = True
	checkData = True
	for lattesId in listIds:
		msgLog = '		> %s => ' %lattesId
		fileXML = pathXML + lattesId + '.xml'
		
		existFile = fun_CheckFile(fileXML)
		if existFile == checkData:
				msgLog = msgLog + 'XML existe'
		else:			
			fileZip = pathZip + lattesId + '.zip'
			
			existFile = fun_CheckFile(fileZip)
			if existFile == checkData:
				funcUnzipCVLattes(pathZip,pathXML,lattesId)
				
				existFile = fun_CheckFile(fileXML)
				if existFile == checkData:
					msgLog = msgLog + 'XML descompactado'
				else:
					msgLog = msgLog + 'XML indisponivel'
					returnData = False
			else:
				msgLog = msgLog + 'ZIP indisponivel'
				returnData = False
							
		modLog.fun_AddLog(fileLog,msgLog);
		
	if returnData == 0:
		print('\n=> Para o programa %s falta(m) curriculo(s), verifique log' %programId)
	
	return returnData

# Monta listas a partir dos dados dos programas onde há programas avaliados
def fun_buildListsGroups(dicHeader):
	listRegionsPPG = []
	del listRegionsPPG[:]
	
	# Abre arquivo do PPG
	fileRead = fun_OpenFile(modLibs.fileListPPG)
	for listData in fileRead:
		listRegionsPPG.append(listData[dicHeader])
	
	listRegionsPPG = list(set(listRegionsPPG))
	listRegionsPPG.remove('')
	listRegionsPPG.sort()
	
	return listRegionsPPG
	
