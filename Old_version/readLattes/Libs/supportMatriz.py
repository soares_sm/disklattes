# -*- coding: utf-8 -*-
"""
####################################################################################################
# @title: nome do módulo
# @Description: Modulo do programa readLattes para ...
#				
#
# @Author: Solon Macedonia Soares
# @Email: soares.solonm@gmail.com
# 
# @Version: 2.0
# @Data_release: xx/xx/20xx
####################################################################################################
"""
######################### PATHS PROGRAM #########################
# PATHS BASE
pathMain = 'C:\\readLattes\\'									# Endereço raiz do programa
pathLibs = pathMain + 'Libs\\'									# Endereço da biblioteca de modulos

######################## IMPORTS MODULOS ########################
# Oficial modules
import numpy													# 

# Support readLattes modules
import supportLibs			as modLibs							# Modulo de acesso a base de dados

# Create matriz
def fun_BuilMatrizInt(listIds):
	matriz = []
	del matriz[:]
	matriz = numpy.zeros((len(listIds),len(listIds)), dtype=numpy.int64)
	
	return matriz
	
def fun_builMatrizFloat(linha,coluna):
	matriz = []
	del matriz[:]
	matriz = numpy.zeros((linha,coluna), dtype=numpy.string)
	
	return matriz
	
def fun_buildMatriz(linha,coluna):
	matriz = []
	del matriz[:]
	
	for i in range(linha):
		matriz.append([''] * coluna)
	
	return matriz