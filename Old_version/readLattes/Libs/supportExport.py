# -*- coding: utf-8 -*-
"""
####################################################################################################
# @title: nome do módulo
# @Description: Modulo do programa readLattes para ...
#				
#
# @Author: Solon Macedonia Soares
# @Email: soares.solonm@gmail.com
# 
# @Version: 2.0
# @Data_release: xx/xx/20xx
####################################################################################################
"""
######################### PATHS PROGRAM #########################
# PATHS BASE
pathMain = 'C:\\readLattes\\'									# Endereço raiz do programa
pathLibs = pathMain + 'Libs\\'									# Endereço da biblioteca de modulos

######################## IMPORTS MODULOS ########################
# Oficial modules
import os														# 
import csv														# 

# Support readLattes modules
import supportLibs			as modLibs							# Modulo de acesso a base de dados

####################### FUNCTIONS PROGRAM #######################
# Definição da função aqui
def printModulo():
	print('supportExport')

	return 0
	
def fun_exportData():
	print('exportar')
	return 0
	
def exportLineCSV(fileCSV,vetor):
	stringLine = str(vetor)
	stringLine = stringLine.replace('[','').replace(']','').replace(',',';').replace("'",'').replace('; ',';')
	writeFileCSV = open(fileCSV, "a+")
	writeFileCSV.write(stringLine)
	writeFileCSV.write("\n")
	writeFileCSV.close()
	
	return 0