# -*- coding: utf-8 -*-
"""
####################################################################################################
# @title: readLattes
# @Description: Programa para a leitura de currículo Lattes e formação de redes de colaboração
#				cientifica
#
# @Author: Solon Macedonia Soares
# @Email: soares.solonm@gmail.com
# 
# @Version: 2.0
# @Data_release: xx/xx/20xx
####################################################################################################
"""
########################### PATHS PROGRAM ###########################
# PATHS BASE
pathMain = 'C:\\readLattes\\'										# Endereço raiz do programa
pathLibs = pathMain + 'Libs\\'										# Endereço da biblioteca de modulos

########################## IMPORTS MODULOS ##########################
# Oficial modules
import sys															# 

# Support readLattes modules
sys.path.append(pathLibs)											# Habilita biblioteca do programa
import supportLibs as modLibs										# Biblioteca de módulos do programa
modDatabase = modLibs.modDatabase									# Módulo para leitura da base de dados
modExport = modLibs.modExport										# Modulo de exportação de dados
modLattes = modLibs.modLattes										# Módulo de suporte as atividades Lattes
modLog = modLibs.modLog												# Módulo de Log do programa
modPPG = modLibs.modPPG												# Módulo de análise da estrutura dos programas
modValid = modLibs.modValid											# Módulo de Validação de dados
modXML = modLibs.modXML												# Módulo de manuseio de XML

############################### FILES ###############################
# FILES
logFileRun = modLibs.logFileRun										# Log de execução do progama
filePPG = modLibs.filePPG											# Arquivo com a lista os programas
fileNotasPPG = modLibs.fileNotasPPG									# Arquivo com a lista de nota separada por ano dos programas
fileListId = modLibs.fileListId										# Arquivo com a lista de Lattes IDs
fileListPPG = modLibs.fileListPPG									# Arquivo com a lista de dados do PPGs

########################## LIST AND MATRIZ ##########################
#listYear = ['2013','2014','2015','2016','2017'];					# Lista os anos avaliados
listYear = ['2017'];										# Lista os anos avaliados para testes controlados
listPPG = []

del listPPG[:]
############################# VARIABLES #############################


"""##############################################################################################"""
############################## PROGRAM ##############################
# Cria log de execução do programa
logFileRun = modLibs.modLog.fun_CreateLogFile(logFileRun)

# Verifica se está disponível os dados gerais dos programas.
msgLog = '\n>>> Identifica arquivos de dados para executar programa.'
modLog.fun_AddLog(logFileRun,msgLog)

modDatabase.fun_DataPPG(filePPG,logFileRun)
modDatabase.fun_DataPPG(fileListId,logFileRun)
modDatabase.fun_DataPPG(fileListPPG,logFileRun)

# Busca arquivo com os programas e o Lattes ID
msgLog = '\n>>> Identificando programa(s), Lattes IDs e Curriculos.'
modLog.fun_AddLog(logFileRun,msgLog)

# Lista quais PPGs estao habilitados para execussao do programa
listPPG = modDatabase.fun_DatabasePPG(filePPG,fileListId,fileListPPG,logFileRun,modLibs.subPathCVlattes,modLibs.subPathZips)

"""#########################"""
"""
# para este programa inicialmente está fechado a avaliação para a seguinte amostragem
# 1. Area: Interdisciplinar
# 2. Subarea: Engenharia, Tecnologia e Gestão
# 3. Modalidade: Separado em duas (Acadêmico e Profissional)
# 4. Observado pore Regiao e por Estado
"""
# Analisa da estrutura dos programas 
msgLog = '\n########## Analisa a estrutura dos programas ##########'
modLog.fun_AddLog(logFileRun,msgLog)
print(msgLog);

dicHeaderPPG = modLibs.dicHeaderPPG									# Cabeçario do arquivo listaDadosPPG.csv
listStruturaType = modLibs.listStruturaType							# Região / UF
listRegions = modDatabase.fun_buildListsGroups(dicHeaderPPG[5])		# busca por Região
listUFs = modDatabase.fun_buildListsGroups(dicHeaderPPG[6])			# busca por UF

listTypeUniversity = modDatabase.fun_buildListsGroups(dicHeaderPPG[2])			# busca por UF

"""
# Busca dados por Região
msgLog = '> Analisa a estrutura ' + listStruturaType[0].upper()
modLog.fun_AddLog(logFileRun,msgLog)
print(msgLog);
modPPG.fun_StruturePPG(listYear,logFileRun,listRegions,modLibs.listStruturaType[0],listStruturaType[0])

# Busca dados por UF
msgLog = '> Analisa a estrutura ' + listStruturaType[1].upper()
modLog.fun_AddLog(logFileRun,msgLog)
print(msgLog);
modPPG.fun_StruturePPG(listYear,logFileRun,listUFs,modLibs.listStruturaType[1],listStruturaType[1])
"""
# Busca dados por administração
msgLog = '> Analisa a estrutura ' + listStruturaType[2].upper()
modLog.fun_AddLog(logFileRun,msgLog)
print(msgLog);
modPPG.fun_StruturePPG(listYear,logFileRun,listTypeUniversity,modLibs.listStruturaType[2],listStruturaType[2])


"""#########################"""
"""
# Analisa os curriculos por ano
msgLog = '\n########## Analisa os curriculos por ano ##########'
modLog.fun_AddLog(logFileRun,msgLog)
print(msgLog);

for runListYear in listYear:
	runYear = runListYear.strip();
	
	# pesquisa dados nos curriculos Lattes
		# Matriz de Adjacencia de Artigos
		# Matriz de Adjacencia de Bancas
		# Destaque da Produção científica
	
	msgLog = '\n######################### '
	msgLog = msgLog + 'Ano observado: ' + runYear
	modLog.fun_AddLog(logFileRun,msgLog)
	print(msgLog);
# end for
"""
# Finaliza log de execução do programa
modLibs.modLog.fun_FinishLogFile(logFileRun)