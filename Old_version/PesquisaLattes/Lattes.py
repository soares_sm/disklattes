import xlrd
import os
import xml.etree.ElementTree as et
from time import sleep

def adiciona(palavra, txt, pos):
	temp = palavra[pos:] # Isso altera o ISSN, ele armazena os ultimos 4 digitos.
	palavra = palavra.replace(palavra[pos:], " @").split()
	palavra = f"{txt}{temp}".join(palavra).replace("@","") # E acrescenta um "-" no meio dele.
	return palavra

def PegaLinha(Caminho, pos):
# CAMINHO, POSIÇÃO
	Documento = xlrd.open_workbook(Caminho) # Indica a pasta a qual o arquivo inicia.
	NomePlanilha = Documento.sheet_names()	# Coleta o documento em excel.
	Planilha = Documento.sheet_by_name(NomePlanilha[pos]) # Pega a planilha com a posição dela.

	return Planilha

def Remove(remove1,texto):
# REMOÇÃO, TEXTO
	texto = str(texto).replace(remove1,"")	# Remove algo da palavra
	return texto

def FatorErro(Fator):
	Fator = str(Fator)
	if(len(Fator) == 6):
		x = Fator[1:]
		Fator = Fator[0] +"."+ x
		Fator = Fator[0:5]
	return Fator

def FatorDeImpacto(ISSNFator,ISSN, FatorTotal, Titulo, FatorTit):
	cont = 0
	for z,x in enumerate(ISSNFator):
		
		for y in x:
			
			y = adiciona(y,"-", 4)
			
			if ISSN == y:
				FatorFinal = FatorTotal[z]
				cont = 1
				break
	if cont == 0:
		for z, x in enumerate(FatorTit):
			if x.lower() == Titulo.lower():
				FatorFinal = FatorTotal[z]
				cont = 1
				break
	if cont == 0:
		FatorFinal = 0

	return FatorFinal

def Escreve(COD_Programa, tituloArtigo, Titulo, ISSN, Ql, Fator):
	text = open("DADOS.csv", "r")
	Cod = text.readlines()
	TitArt = text.readlines()
	tit = text.readlines()
	Iss = text.readlines()
	Quals = text.readlines()
	Fat = text.readlines()

	Cod.append(f"{COD_Programa};")
	TitArt.append(f"{tituloArtigo};")
	tit.append(f"{Titulo};")
	Iss.append(f"{ISSN};")
	Quals.append(f"{Ql};")
	Fat.append(f"{Fator};")

	text = open("DADOS.csv", "w")

	text.writelines(Cod)
	text.writelines(TitArt)
	text.writelines(tit)
	text.writelines(Iss)
	text.writelines(Quals)
	text.writelines(Fat)
	text.writelines("\n")
	
	text.close()

def ColetaDeDados(QualiS,FatorI, Document):
# Pegando Qualis da revista
	PlanilhaQu = PegaLinha(QualiS, 0) # Coleta o caminho da planilha.
	LinhasQu = PlanilhaQu.nrows # Coleta a quantidade de linhas que ela possui.
	Qualis = [] 
	Titulos = []
	ISSNs = []

	for x in range(LinhasQu):
		if x != 0:
			ISSNs.append(Remove("'",Remove("text:",PlanilhaQu.cell(x, 0))))

			Qualis.append(float(str(Remove("number:",Remove("'",PlanilhaQu.cell(x,1))))))
			
			T = Remove("text:'",PlanilhaQu.cell(x, 2))
			Titulos.append(T[:(len(T)-1)])	# Coloca todas as informações um uma lista, e é formatada.
# Fim da coleta.

# Pegando o Fator de Impacto da revista.
	PlanilhaFI = PegaLinha(FatorI, 0)
	LinhasFI = PlanilhaFI.nrows
	FI = []
	FIIn = []
	FatorTit = []

	for x in range(LinhasFI):
		if x != 0:
			FI.append(float(str(PlanilhaFI.cell(x,5)).replace("text:", "").replace("'", "").replace("number:","").replace("empty:","0").replace(".0", "").replace(",", ".")))
		
			FIIn.append(str(PlanilhaFI.cell(x,4)).replace("text:", "").replace("'", "").replace("number:","").replace(".0", "").replace(",", "").split())
			
			T = str(PlanilhaFI.cell(x,2)).replace("text:'", "").replace('"text:"',"")
			
			FatorTit.append(T[:(len(T)-1)])
# Fim da coleta.

# Inicia o XML.
	Pasta = os.path.dirname(os.path.realpath(__file__))	# Indica a pasta atual.
	Arquivo = os.path.join(Pasta, Document) # Indica qual é o arquivo.
	Arvore = et.parse(Arquivo)
	Raiz = Arvore.getroot()
# Fim do XML.

	ResultadoFinal = []
# Identificando se corresponde.
	for Inicio in Raiz.findall("PRODUCAO-BIBLIOGRAFICA"):
		for Tronco in Inicio.findall("ARTIGOS-PUBLICADOS"):
			for Elem in Tronco.findall("ARTIGO-PUBLICADO"): #	Inicia as buscas.
				auxilio = 0
				
				COD_Programa = "33092010016P3"
				tituloArtigo = Elem.find("DADOS-BASICOS-DO-ARTIGO").get("TITULO-DO-ARTIGO").replace(";","-")

				Titulo = Elem.find("DETALHAMENTO-DO-ARTIGO").get("TITULO-DO-PERIODICO-OU-REVISTA")
				
				ISSN = adiciona(Elem.find('DETALHAMENTO-DO-ARTIGO').get('ISSN'), "-", 4)
				print("!"*10)
				print(tituloArtigo)
				for loop, Sub in enumerate(ISSNs):	# Indica a autênticidade do artigo em relação à revista com base no ISSN.
					if Sub == ISSN:
						auxilio = 1
						Ql = Qualis[loop]
						Fator = FatorErro(FatorDeImpacto(FIIn, ISSN, FI, Titulo, FatorTit))
						break
						
				if auxilio == 0:

					for loop, Sub in enumerate(Titulos):	# Indica a autênticidade do artigo em relação à revista com base no Titulo.
						if Sub.lower() == Titulo.lower():
							auxilio = 1
							Ql = Qualis[loop]
							ISSN = ISSNs[loop]
							Fator = FatorErro(FatorDeImpacto(FIIn,ISSN,FI, Titulo, FatorTit))
							break

				if auxilio == 0:	#	Se não conseguir identificar.
					Fator = 0
					Ql = 0
				
				print(Titulo)
				print("!"*10)
				Escreve(COD_Programa, tituloArtigo, Titulo, ISSN, Ql, Fator)
	return
# Fim da identificação

