# -*- coding: utf-8 -*-
"""
###########################################################
# @Description: Programa para a leitura de curriculo Lattes
#				e formação de redes de colaboração
#				cientifica
# @Author: Solon Macedonia Soares
# @Email: soares.solonm@gmail.com
# 
# Version: 1.1
# Data release: 03/10/2019
###########################################################
"""
# Oficial modules
#from datetime import datetime
from xml.dom.minidom import parse
import xml.dom.minidom
import sys
import os
#import imp
#import zipfile
import csv

# readLattes modules
sys.path.append('C:\\PesquisaLattes\\Libs\\')			# Library

# Support modules
import supportLog as modLog									# File Logs Functions

########################## PATHS ##########################
# PATHS
pathMain = "C:\\PesquisaLattes\\";							# Path Main
pathXML = pathMain + "Lattes\\";							# Path CV Lattes
pathLog = pathMain + "Logs\\";								# Path Logs
pathMatriz = pathMain + "Matrizes\\";						# Path Matriz Adjacent
pathPPG = pathMain + "PPGs\\";								# Path Teacher lists
pathZip = pathMain + "Zips\\"								# Path PPGs

########################## FILES #########################
# FILES
filePPG = pathMain + "PPGs.txt";							# File with PPG list to run
fileListId = pathPPG + "yearWorks.csv"						# File with ID list by year to run
#fileListPPG = pathMain + "Lista_PPGs.txt"					# File with PPG data list
fileListPPG = pathPPG + "listDataPPG.csv"					# File with PPG data list
fileXML = "";												# File curriculum Lattes XML
fileCSV = "";												# File csv export
fileZIP = "";												# File zip with curriculum Lattes
fileAuthorPapers = pathMatriz + "list_papers_authors.csv"	# File to export analises author Papers
fileExaminingBoards = pathMatriz + "list_examining_boards.csv"	# File to export analises author Papers

######################### PROGRAM #########################
listYear = ["2013","2014","2015","2016","2017"];			# List
#fileExit = pathMatriz + "dados_banca.csv"
fileExit = pathMatriz + "dados_artigo.csv"
msgLog = ""
runYear = ""

for runListYear in listYear:
	msgLog = msgLog + "Ano observado: " + str(runYear)
	runYear = runListYear.strip();
	
	
	listFiles = os.listdir(pathMatriz + str(runYear) + '\\')
	for listFile in listFiles:
		print listFile
		fileTeste = pathMatriz + str(runYear) + "\\" + listFile

		cont = 0
		count = 0
		soma = 0
		valorMin = 0
		valorMax = 0

		with open(fileTeste) as readFile:
			readFile = csv.reader(readFile, delimiter=';', quoting=csv.QUOTE_NONE)
			x = 0
			for listDataFile in readFile:
				cont = cont + 1
				dataFile = listDataFile
				
				if cont == 1:
					qtdDoc = int(dataFile[0])
				elif cont > 1 and cont <= (qtdDoc + 1):
					somaK = 0
					for x in range(0,qtdDoc,1):
						valor = int(dataFile[x])
						somaK = somaK + valor
						soma = soma + valor
						count = + 1
					if valorMax < somaK:
						valorMax = somaK
					if valorMin > somaK:
						valorMin = somaK
				elif cont == (qtdDoc+5):
					codProgram = dataFile[1]
				elif cont == (qtdDoc+6):
					conceptProgram = dataFile[1]
			
			soma = soma / 2
		
		fileData = str(runYear) + ";" + str(qtdDoc) + ";" + codProgram + ";" + str(conceptProgram) + ";" + str(soma) + ";" + str(valorMin) + ";" + str(valorMax)		
		print "	> " + fileData
		
		modLog.funcAddData(fileExit,fileData)
		
		#del fileData[:]