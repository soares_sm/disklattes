from glob import glob
from Lattes import *

text = open("DADOS.csv", "w")

text.write("COD_Programa;")
text.write("Titulo do Artigo;")
text.write("Titulo da Revista;")
text.write("ISBN;")
text.write("Qualis;")
text.write("Fator;")
text.write("\n")

text.close()

for f in glob('DataBase/CVLattes/*'):
	
	f = Remove('DataBase/CVLattes\\',f)

	print(f)
	ColetaDeDados('DataBase/Excel/a.xlsx', 'DataBase/Excel/FatordeImpacto2018.xlsx', f'DataBase/CVLattes/{f}')
