# -*- coding: utf-8 -*-
"""
###########################################################
# @Description: Programa para a leitura de curriculo Lattes
#				e formação de redes de colaboração
#				cientifica
# @Author: Solon Macedonia Soares
# @Email: soares.solonm@gmail.com
# 
# Version: 1.1
# Data release: 03/10/2019
###########################################################
"""
######################### IMPORTS #########################
# Oficial modules
#from datetime import datetime
from xml.dom.minidom import parse
import xml.dom.minidom
import sys
import os
#import imp
#import zipfile
import csv

# readLattes modules
sys.path.append('C:\\PesquisaLattes\\Libs\\')			# Library

# Support modules
import supportLattes as modLattes							# Generic Functions
import supportListPPG as modPPG								# Work with data files to run program
import supportLog as modLog									# File Logs Functions
import supportMatriz as modMatriz							# Matriz and Vectos Functions
#import supportNLS as modNLS								# NSL Functions
#import supportValidation as modValida						# Validation Functions
import supportXML as modXML									# XML Functions

# Analises modules
import analyseCoauthoshipPapers as modCoauthorship			# Analise the coauthorship network
import analyseExaminingBoard as modBoard 					# Analise the examining board network

########################## PATHS ##########################
# PATHS
pathMain = "C:\\PesquisaLattes\\";							# Path Main
pathXML = pathMain + "Lattes\\";							# Path CV Lattes
pathLog = pathMain + "Logs\\";								# Path Logs
pathMatriz = pathMain + "Matrizes\\";						# Path Matriz Adjacent
pathPPG = pathMain + "PPGs\\";								# Path Teacher lists
pathZip = pathMain + "Zips\\"								# Path PPGs
#
########################## FILES #########################
# FILES
filePPG = pathMain + "PPGs.txt";							# File with PPG list to run
fileListId = pathPPG + "yearWorks.csv"						# File with ID list by year to run
#fileListPPG = pathMain + "Lista_PPGs.txt"					# File with PPG data list
fileListPPG = pathPPG + "listDataPPG.csv"					# File with PPG data list
fileXML = "";												# File curriculum Lattes XML
fileCSV = "";												# File csv export
fileZIP = "";												# File zip with curriculum Lattes
fileAuthorPapers = pathMatriz + "list_papers_authors.csv"	# File to export analises author Papers
fileExaminingBoards = pathMatriz + "list_examining_boards.csv"	# File to export analises author Papers

########################### LOG ###########################
# LOGS
logFile = "";												# Log 
logFilePPG = "";											# Log PPG run
logFileRun = "";											# Log run program

nextLine = "\n"
checkData = 1;												# TRUE

###################### LIST AND MATRIZ ####################
# Lists and Matriz
runListYear = [];											# List Year run program
listYear = ["2013","2014","2015","2016","2017"];			# List
#listYear = ["2015"];										# List

listAuthor = []												# 
listCoauthor = []											# 
listFileCSV = []											# List file csv used in network exported
listFiles = []												#
listDataPPG = []											# List PPG data
listDataTeacher = []										# 
listLattesIds = []											# 
listPPG = []												# List 
listRunDataCSV = []											#
listUpdateFileMatriz = []									# 
listDataProgram = []
listHeader = []
#listReturnData = []
montalistaPPG = []
matrizAdjacente = [];										# Matriz Adjacent
matrizAlterada = [];										# 

########################## VARIABLES ########################
msgLog = ""													# Mensage to file logs

######################### PROGRAM #########################
# Cria log de execucao do programa
logFileRun = pathLog + "LOG_RUN_PROGRAM_" + modLog.funcDateFileLog() + ".txt"
msgLog = modLog.funcCreateLogFile(logFileRun);
print(msgLog)

# Verifica se está disponível os dados gerais dos programas.
msgLog = "\n>>> Identifica arquivos de dados para executar programa."
modLog.funcAddLog(logFileRun,msgLog);
print(msgLog);

modLattes.funcDataPPG(filePPG,logFileRun);
modLattes.funcDataPPG(fileListId,logFileRun);
modLattes.funcDataPPG(fileListPPG,logFileRun);

# Busca arquivo com os programas e o Lattes ID
msgLog = "\n>>> Identificando programa(s), Lattes IDs e Curriculos"
modLog.funcAddLog(logFileRun,msgLog);
print(msgLog);

#lista quais PPGs estao habilitados para execussao do programa
listPPG = modLattes.funcListPPG(filePPG,fileListPPG,logFileRun,fileListId,pathXML,pathZip);

msgLog = "\n>>> Todos os dados disponiveis, iniciando analise dos programas."
modLog.funcAddLog(logFileRun,msgLog);
print(msgLog);

listHeader = modCoauthorship.funcBuildheader()
modCoauthorship.funcAbstractDataPapersCSV(fileAuthorPapers,listHeader)

del montalistaPPG[:]
#Roda todos os dados por ano
for runListYear in listYear:
	runYear = runListYear.strip();
	del listHeader[:]
	
	msgLog = "\n#########################\n"
	msgLog = msgLog + "Ano observado: " + runYear
	modLog.funcAddLog(logFileRun,msgLog);
	print(msgLog);

	#analises network collaboration
	del listFiles[:]
	listFiles.append(logFileRun)
	listFiles.append(fileListId)
	listFiles.append(fileListPPG)
	listFiles.append(pathMatriz)
	listFiles.append(pathXML)
	listFiles.append(fileAuthorPapers)
	listFiles.append(fileExaminingBoards)
	
	#del listRunDataCSV[:]
	
	#listReturnData = modCoauthorship.funcCoauthoshipPapers(listFiles,runYear,listPPG,listRunDataCSV)
	#listRunDataCSV = listReturnData[0]
	#if len(listReturnData[1]) > 0:
	#	listDataProgram.append(listReturnData[1])
	#	montalistaPPG = listReturnData[2]
	
	listReturnData = modBoard.funcExaminingBoard(listFiles,runYear,listPPG,listRunDataCSV)
	listRunDataCSV = listReturnData[0]
	if len(listReturnData[1]) > 0:
		listDataProgram.append(listReturnData[1])
		montalistaPPG = listReturnData[2]
	

modLattes.funcExportDataRunCSV(pathMatriz,listRunDataCSV,logFileRun)
	
# Finalisa o log e o programa
modLog.funcFinishLogFile(logFileRun)