# -*- coding: utf-8 -*-
"""
###########################################################
# @Description: Modulo para formar listas de Ids dos
#				PPGs 
#
# @Author: Solon Macedonia Soares
# @Email: soares.solonm@gmail.com
# 
# Version: 1.2
# Data release: xxxx/xx/xx
###########################################################
"""
######################### IMPORTS #########################
# Oficial modules
#from datetime import datetime
#from xml.dom.minidom import parse
#import xml.dom.minidom
import sys
#import os
#import imp
#import zipfile
import csv

# Personal modules
sys.path.append('C:\\PesquisaLattes\\Libs\\')				# Library
#import supportLattes as modLattes							# Generic Functions
#import supportLog as modLog								# File Logs Functions
#import supportMatriz as modMatriz							# Matriz and Vectos Functions
#import supportNLS as modNLS								# NSL Functions
#import supportValidation as modValida						# Validation Functions
#import supportXML as modXML								# XML Functions


######################## FUNCTIONS #########################
# Open csv file
def funcOpenFile(fileListId):
	fileRead = csv.DictReader(open(fileListId, 'rb'), delimiter=';', quoting=csv.QUOTE_NONE)
	return fileRead

# List Lattes Id by PPG
def funcListIdsPPG(fileListId,codePPG):
	listIds = []
	del listIds[:]
	fileRead = funcOpenFile(fileListId)
	for listData in fileRead:
		if codePPG == listData["ppg"]:
			listIds.append(listData["id"])
	
	listIds = list(set(listIds))
	listIds.sort()
	return listIds

# List Lattes Id by PPG and year
def funcListIdsYearPPG(fileListId,codePPG, yearCheck):
	listIds = []
	del listIds[:]
	fileRead = funcOpenFile(fileListId)
	for listData in fileRead:
		if codePPG == listData["ppg"] and yearCheck == listData["ano"]:
			listIds.append(listData["id"])
	
	listIds = list(set(listIds))
	listIds.sort()
	return listIds

# Check data of the PPGs
def funcCheckDataPPG(fileListPPG,programId):
	checkPPG = 0
	
	fileRead = funcOpenFile(fileListPPG)
	for listData in fileRead:
		if programId == listData["ppg"]:
			checkPPG = 1
	
	return checkPPG

def funcListDataPPG(fileListPPG,programId):
	listDataPPG = []
	del listDataPPG [:]
	
	fileRead = funcOpenFile(fileListPPG)
	for listData in fileRead:
		if programId == listData["ppg"]:
			listDataPPG.append(listData["ppg"])
			listDataPPG.append(listData["sigla"])
			listDataPPG.append(listData["nota"])
			listDataPPG.append("")
			if listData["adm"] == "PARTICULAR":
				listDataPPG.append("PRIVADA")
			else:
				listDataPPG.append(listData["adm"])
	
	return listDataPPG