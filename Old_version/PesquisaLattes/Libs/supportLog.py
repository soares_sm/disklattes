# -*- coding: utf-8 -*-
##################################################
# @Description: 
# @Author: Solon Macedonia Soares
# @Email: soares.solonm@gmail.com
# 
# Version: 1.0
# Data release: mm/dd/yyyy
##################################################

###### IMPORT MODULES
# Oficial modules
from datetime import datetime
import sys
import csv

# Personal modules
sys.path.append('C:\\PesquisaLattes\\Libs\\')

######################### FUNCTIONS #########################
# Datetime to file name
def funcDateFileLog():
	dateNow = datetime.now();
	dateToFile = dateNow.strftime('%Y%m%d_%H%M%S');
	return dateToFile

# Datetime used with data in the log
def funcDatetimeDefined():
	dateNow = datetime.now();
	dateNow = dateNow.strftime('%d/%m/%Y %H:%M:%S');
	return dateNow

# Create LOG File
def funcCreateLogFile(logFile):
	msgLog = ("##### Inicio: " + funcDatetimeDefined() + " #####");
	dataLog = open(logFile, "w");
	dataLog.write(msgLog);
	dataLog.close();
	return msgLog

# Finish LOG File
def funcFinishLogFile(logFile):
	msgLog = ("\n##### Termino: " + funcDatetimeDefined() + " #####");
	funcAddLog(logFile,msgLog);
	print(msgLog)
	
	print("Arquivo de LOG: " + logFile)
	exit()
	return 0
	
# Add information in LOG File
def funcAddLog(logFile,msgLog):
	dataInFile = funcGetDataInFile(logFile);
	fLog = open(logFile, "w");
	fLog.write(dataInFile);
	fLog.write("\n");
	fLog.write(msgLog);
	fLog.close();
	return 0

# Get old information in LOG File	
def funcGetDataInFile(logFile):
	fLogData = open(logFile, "r");
	dataInFile = fLogData.read();
	fLogData.close();
	return dataInFile

def funcAddData(fileExit,fileData):
	#dataInFile = funcGetDataInFile(fileExit);
	fLog = open(fileExit, "a+");
	#fLog.write(dataInFile);
	fLog.write("\n");
	fLog.write(fileData);
	fLog.close();
	
	return 0