# -*- coding: utf-8 -*-
"""
###########################################################
# @Description: Modulo para formar listas de Ids dos
#				PPGs 
#
# @Author: Solon Macedonia Soares
# @Email: soares.solonm@gmail.com
# 
# Version: 1.2
# Data release: xxxx/xx/xx
###########################################################
"""
######################### IMPORTS #########################
# Oficial modules
import sys
import csv

# readLattes modules
sys.path.append('C:\\PesquisaLattes\\Libs\\')				# Library

# Support modules
import supportLattes as modLattes							# Generic Functions
import supportListPPG as modPPG								# Work with data files to run program
import supportLog as modLog									# File Logs Functions
import supportMatriz as modMatriz							# Matriz and Vectos Functions
import supportXML as modXML									# XML Functions

######################## FUNCTIONS #########################
def funcCoauthoshipPapers(listFiles,runYear,listPPG,listRunDataCSV):
	listAuthor = []											# 
	listCoauthor = []										# 
	listDataPPG = []										# 
	listDataTeacher = []									# 
	listFileCSV = []										# 
	listIds = []											# 
	listLattesIds = []										# 
	qtdPaperRerturn = []									# 
	listDataCoauthorship = []								# 
	listDataAnaliseProgram = []								# 
	qtdUniqueAuthor = []									# 
	montalistaPPG = []										# 
	montaLinhaID = []										# 
	
	uniqueTitle = []										# junta todos os títulos dos artigos de um programa
	
	del qtdUniqueAuthor[:]
	del listDataAnaliseProgram[:]	
	del listFileCSV[:]
	del listDataCoauthorship[:]
	del montalistaPPG[:]
	del montaLinhaID[:]
	
	fileLog = listFiles[0]
	fileListPPG = listFiles[2]
	fileListId = listFiles[1]
	fileAuthorPapersy = listFiles[5]
	
	pathMatriz =  listFiles[3]
	pathXML =  listFiles[4]
	
	sumPapers = 0
	sumAuthors = 0
	sumCoauthorship = 0
	
	msgLog = "> Coautoria"
	modLog.funcAddLog(fileLog,msgLog);
	print(msgLog);

	for programList in listPPG:
		matrizAdjacente = []									#
	
		del uniqueTitle[:]										# limpa a lista de artigos de um programa.
		del listAuthor[:]
		del listCoauthor[:]
		del listDataPPG[:]
		del listDataTeacher[:]
		del listLattesIds[:]
		del matrizAdjacente[:]
		
		contTeacher = 0										# Contagem da linha da matriz
		programId = programList.strip()
		totalPapers = 0
		totalAutores = 0
		totalCoautores = 0		
		
		#lista dados do PPG para exportacao
		listDataPPG = modPPG.funcListDataPPG(fileListPPG,programId);
		listDataPPG[3] = runYear
		
		msgLog = "	> Programa: " + programId
		modLog.funcAddLog(fileLog,msgLog);
		print(msgLog);
		
		#lista docentes do ano observado
		listLattesIds = modPPG.funcListIdsYearPPG(fileListId,programId,runYear)	
		
		#monta matriz de adjacencia
		matrizAdjacente = modMatriz.funcBuilMatriz(listLattesIds)
		
		if len(matrizAdjacente) > 0:
			msgLog = "		> Criando Matriz de Adjacencia base para todos os programas."
			listDataPPG.append(len(matrizAdjacente))
			msgLog = msgLog + " " + str(len(matrizAdjacente)) + "x" + str(len(matrizAdjacente))
			modLog.funcAddLog(fileLog,msgLog)
			print(msgLog)		
			
			#Monta a lista de autores para validadar a partir da lista de IDs
			for lattesID in listLattesIds:
				fileXML = pathXML + lattesID + ".xml"
				listAuthor = modXML.funcBuilDataTeacher(fileXML,lattesID)
				listCoauthor.append(listAuthor)
			
			msgLog = "		> Analise do lattes."
			modLog.funcAddLog(fileLog,msgLog)
			print(msgLog)
			
			#Monta lista com dados do autor para validar
			for teacherId in listLattesIds:
				returnPapers = 0
				returnAutores = 0
				returnCoautores = 0
				
				fileXML = pathXML + teacherId + ".xml"
				listDataTeacher = modXML.funcBuilDataTeacher(fileXML,teacherId);
				
				msgLog = "			> Lattes ID: " + teacherId
				modLog.funcAddLog(fileLog,msgLog)
				#print(msgLog)
				print(uniqueTitle)
				print(teacherId)
				
				"""
				# Usado para diagnosticar erros de validação de dados
				msgLog = "				> NOME: " + listDataTeacher[0];
				modLog.funcAddLog(fileLog,msgLog);
				print(msgLog);
				
				msgLog = "				> Citação: " + listDataTeacher[2]);
				modLog.funcAddLog(fileLog,msgLog);
				print(msgLog);
				
				msgLog = "				> Posição do Autor: " + str(contTeacher));
				modLog.funcAddLog(fileLog,msgLog);
				print(msgLog);
				"""
				
				""" validando este ponto"""
				returnMatrizAdjacente = modXML.funcProdBibliograficaCoautor(fileXML,fileLog,runYear,listDataTeacher,listCoauthor,contTeacher,matrizAdjacente,qtdPaperRerturn,uniqueTitle,programId)
				matrizAdjacente = returnMatrizAdjacente[0]				# Matriz Adjacente
				returnPapers = returnMatrizAdjacente[1]					# Quantidades de Papers únicos
				returnAutores = returnMatrizAdjacente[2]				# 
				returnCoautores = returnMatrizAdjacente[3]				# 
				qtdPaperRerturn = returnMatrizAdjacente[4]				# 
				qtdAuthorNetwork  = returnMatrizAdjacente[5]			# 				
				
				uniqueTitle = returnMatrizAdjacente[6]
				TotalArtigosUnicos = len(uniqueTitle)
				
				totalPapers = totalPapers + len(qtdPaperRerturn)
				totalAutores = totalAutores + returnAutores
				totalCoautores = totalCoautores + returnCoautores
				
				contTeacher = contTeacher + 1
				print("	>> Retorno do total de artigos: " + str(len(qtdPaperRerturn)))
				print("	>> Artigos acumulados: " + str(totalPapers))
				print("	>> Novacontagem de artigos unicos: " + str(TotalArtigosUnicos))
				if totalPapers == 0:
					msgLog = "				> Sem artigos para analise"
					modLog.funcAddLog(fileLog,msgLog)
					
			# FIM do LOOP (for teacherId in listLattesIds:)
			"""
			#Use to validade data
			middleAuthors = totalAutores / totalPapers
			print("\n#####")
			msgLog = "Quantidade total de Papers: " + str(totalPapers)
			modLog.funcAddLog(fileLog,msgLog)
			print(msgLog)
			msgLog = "Total de Autores: " + str(totalAutores)
			modLog.funcAddLog(fileLog,msgLog)
			print(msgLog)
			msgLog = "Total de Papers com Coautoria: " + str(totalCoautores)
			modLog.funcAddLog(fileLog,msgLog)
			print(msgLog)
			#msgLog = "Media de Autores: " + str(middleAuthors)
			#modLog.funcAddLog(fileLog,msgLog)
			#print(msgLog)
			"""
			
			# Media de artigo por Docente
			if totalPapers > 0 and len(matrizAdjacente) > 0:
				meadTeacherPaper = float(totalPapers)/float(len(matrizAdjacente))
			else:
				meadTeacherPaper = float(0.0)
			# Media de artigo por Autor
			if totalPapers > 0 and len(qtdAuthorNetwork) > 0:
				meadPaperAuthors = float(totalPapers)/float(len(qtdAuthorNetwork))
			else:
				meadPaperAuthors = float(0.0)
			# Media de Autores por artigo
			if totalAutores > 0 and totalPapers > 0:
				meadAuthorsPaper = float(totalAutores)/float(totalPapers)
			else:
				meadAuthorsPaper = float(0.0)
			# Media de coautores por artigo
			if totalCoautores > 0 and totalPapers > 0:
				meadCoauthorshipPaper = float(totalCoautores)/float(totalPapers)
			else:
				meadCoauthorshipPaper = float(0.0)
			
			del listDataAnaliseProgram[:]
			listDataAnaliseProgram.append(listDataPPG[3])								# ano
			listDataAnaliseProgram.append(listDataPPG[1])								# sigla
			listDataAnaliseProgram.append(listDataPPG[0])								# programa
			listDataAnaliseProgram.append(listDataPPG[2])								# conceito
			listDataAnaliseProgram.append(listDataPPG[4])								# administracao
			listDataAnaliseProgram.append(str(len(matrizAdjacente)))					# Docentes
			listDataAnaliseProgram.append(str(len(qtdAuthorNetwork)))					# Autor
			listDataAnaliseProgram.append(str(totalCoautores))							# Coautores
			listDataAnaliseProgram.append(str(totalAutores))							# Total-Autores
			listDataAnaliseProgram.append(str(totalPapers))								# Artigos
			listDataAnaliseProgram.append("%0.4f" %meadTeacherPaper)					# Media-Artigo-Docente
			listDataAnaliseProgram.append("%0.4f" %meadPaperAuthors)					# Media-Artigo-Autor
			listDataAnaliseProgram.append("%0.4f" %meadCoauthorshipPaper)				# Media-Coautores-Artigo
			listDataAnaliseProgram.append("%0.4f" %meadAuthorsPaper)					# Media-Autores-Artigo
			listDataAnaliseProgram.append("-")											# CC
			listDataAnaliseProgram.append("-")											# SPL
			listDataAnaliseProgram.append("-")											# DEN
			
			funcAddDataPapersCSV(fileAuthorPapersy,listDataAnaliseProgram)
			
			############################
			# Verifica como sera tratado a exportacao da matriz de adjacencia
			fileCSV = "ARTIGO_" + listDataPPG[1] + "_" + listDataPPG[0] + "_C" + listDataPPG[2] + "_" + listDataPPG[3] + ".csv"
			listRunDataCSV.append("dados\\" + "\\" + runYear + "\\" + "\\" + fileCSV)
			fileCSV = pathMatriz + runYear + "\\" + fileCSV					
			msgLog = modLattes.funcCheckFileMatrizPPG(fileCSV)
			modLog.funcAddLog(fileLog,msgLog);
			print(msgLog);
			############################
			
			#exporta matriz atualizada
			#funcExportMatrizCSV(fileCSV,matrizAdjacente,listDataPPG);
			funcExportMatrizCSV(fileCSV,matrizAdjacente,listDataPPG);
			msgLog = "		> Matriz atualizada"
			modLog.funcAddLog(fileLog,msgLog);
			print(msgLog);
		else:
			msgLog = "		> Programa sem pesquisadores para este ano: " + runYear
			modLog.funcAddLog(fileLog,msgLog);
			print(msgLog)
		
	
	listDataCoauthorship.append(listDataAnaliseProgram)
		#modLog.funcFinishLogFile(fileLog)
	# fim do for	

	return listRunDataCSV,listDataCoauthorship,montalistaPPG
		
def funcExportMatrizCSV(fileCSV,matrizAdjacente,listDataPPG):
	matrizImpressao = []
	linha = []
	del matrizImpressao[:]
	del linha[:]
	
	dataLine = []
	del dataLine[:]
	
	tamMatriz = str(len(matrizAdjacente))
	writeFileCSV = csv.writer(open(fileCSV, "wb"), delimiter=';')
	writeFileCSV.writerow([tamMatriz,""])
	
	for lineMatriz in matrizAdjacente:
		del dataLine[:]
		for coluna in lineMatriz:
			dataLine.append(int(coluna))
		dataLine.append("")
		#print(dataLine)
		writeFileCSV.writerow(dataLine);
		
	writeFileCSV.writerow(["dados",""	])
	writeFileCSV.writerow(["ano",listDataPPG[3],""])
	writeFileCSV.writerow(["IES",listDataPPG[1],""])
	writeFileCSV.writerow(["curso",listDataPPG[0],""])
	writeFileCSV.writerow(["conceito",listDataPPG[2],""])
	#writeFileCSV.writerow(["administracao",listDataPPG[4],""])
	#writeFileCSV.writerow(["grau",listDataPPG[7],""])
	#writeFileCSV.writerow(["regiao",listDataPPG[5],""])
	#writeFileCSV.writerow(["uf",listDataPPG[6],""])
	return 0
	
def funcBuildheader():
	listHeader = []
	del listHeader[:]
	
	listHeader.append("ano")
	listHeader.append("sigla")
	listHeader.append("programa")
	listHeader.append("conceito")
	listHeader.append("administracao")
	listHeader.append("Docentes")
	listHeader.append("Autor")
	listHeader.append("Coautores")
	listHeader.append("Total-Autores")
	listHeader.append("Artigos")
	listHeader.append("Media-Artigo-Docente")
	listHeader.append("Media-Artigo-Autor")
	listHeader.append("Media-Coautores-Artigo")
	listHeader.append("Media-Autores-Artigo")
	listHeader.append("CC")
	listHeader.append("SPL")
	listHeader.append("DEN")
	
	return listHeader
	
def funcAbstractDataPapersCSV(fileCSV,header):
	writeFile = open(fileCSV, "w")
	lineWrite = str(header).replace("[","").replace("]","").replace("'","").replace(",",";").replace(" ","")
	lineWrite = str(lineWrite).replace(" ","")
	writeFile.write(lineWrite)
	writeFile.write("\n")
	
	return 0

def funcAddDataPapersCSV(fileCSV,dataCSV):
	writeFile = open(fileCSV, "a+")
	lineWrite = str(dataCSV).replace("[","").replace("]","").replace("'","").replace(",",";")
	lineWrite = str(lineWrite).replace(" ","")
	writeFile.write(lineWrite)
	writeFile.write("\n")
	
	return 0