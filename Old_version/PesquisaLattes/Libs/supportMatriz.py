# -*- coding: utf-8 -*-
##################################################
# @Description: 
# @Author: Solon Macedonia Soares
# @Email: soares.solonm@gmail.com
# 
# Version: 1.0
# Data release: mm/dd/yyyy
##################################################

###### IMPORT MODULES
# Oficial module
import sys
import time
import numpy

# Personal modules
sys.path.append('C:\\PesquisaLattes\\Libs\\')

# Create matriz
def funcBuilMatriz(listIds):
	matriz = []
	del matriz[:]
	matriz = numpy.zeros((len(listIds),len(listIds)), dtype=numpy.int64)
	
	return matriz