# -*- coding: utf-8 -*-
##################################################
# @Description: 
# @Author: Solon Macedonia Soares
# @Email: soares.solonm@gmail.com
# 
# Version: 1.0
# Data release: mm/dd/yyyy
##################################################

def funcValidaDados(checkData,listaValidar):
	auxCont = 0;
	returnData = -1;
	for lista in listaValidar:
		if checkData in lista:
			returnData = auxCont
		auxCont = auxCont + 1;
	
	return returnData

def funcValidaCitacao(checkData,listaValidar):
	auxCont = 0;
	returnData = -1;
	for listaX in listaValidar:
		for lista in listaX:
			if checkData in lista:
				returnData = auxCont
		auxCont = auxCont + 1
	
	return returnData

def funcInsertUniqueTitle(titlePaper,newspaperName,newspaperId,uniqueTitle):
	print("		> Quantidade de artigos antes da validacao: " + str(len(uniqueTitle)))
	if titlePaper not in uniqueTitle:
		uniqueTitle.append(titlePaper)
		#vetorNota.append(revistaQualis(revista))
	print("		> Quantidade de artigos depois da validacao: " + str(len(uniqueTitle)))
	return uniqueTitle

def funcInsertUniqueID(idInsert,uniqueID):
	if idInsert not in uniqueID:
		uniqueID.append(idInsert)
	return uniqueID
	
def funcIdentifyAuthorID(listCoauthor,dataValida,uniqueID):
	valorReturn = ""
	identifyLine = listCoauthor[dataValida]
	idIdentify = identifyLine[1]
	
	uniqueID = funcInsertUniqueID(idIdentify,uniqueID)
	
	return uniqueID
	
