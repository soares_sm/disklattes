# -*- coding: utf-8 -*-
##################################################
# @Description: 
# @Author: Solon Macedonia Soares
# @Email: soares.solonm@gmail.com
# 
# Version: 1.0
# Data release: mm/dd/yyyy
##################################################

##################### IMPORTS #####################
# Oficial modules
from xml.dom.minidom import parse
import xml.dom.minidom
import sys
import os
import codecs

# Personal modules
sys.path.append('C:\\PesquisaLattes\\Libs\\')
import supportLog as modLog									# File Logs Functions
import supportMatriz as modMatriz							# Matriz and Vectos Functions
import supportValidation as modValida						# Validation Functions

def funcCvLattesAnalisa(openXML):
	DOMTree = xml.dom.minidom.parse(openXML);
	cvLattesAnalisa = DOMTree.documentElement;
	return cvLattesAnalisa

def funcDadosGerais(fileXML):
	tagName = "DADOS-GERAIS"
	cvLattesAnalisa = funcCvLattesAnalisa(fileXML)
	dadosGerais = cvLattesAnalisa.getElementsByTagName(tagName);
	return dadosGerais

def funcBuilDataTeacher(fileXML,lattesId):
	dataTeacher = []
	del dataTeacher[:]
	
	dadosGerais = funcDadosGerais(fileXML)
	for dadosGeraisX in dadosGerais:
		teacherName = dadosGeraisX.getAttribute("NOME-COMPLETO").upper();
		listCitation = dadosGeraisX.getAttribute("NOME-EM-CITACOES-BIBLIOGRAFICAS");
	dataTeacher = [teacherName,lattesId,listCitation]
	return dataTeacher

def funcProdBibliografica(fileXML):
	tagName1 = "PRODUCAO-BIBLIOGRAFICA"
	tagName2 = "ARTIGOS-PUBLICADOS"
	tagName3 = "ARTIGO-PUBLICADO"
	cvLattesAnalisa = funcCvLattesAnalisa(fileXML)
	prodBibliografica = cvLattesAnalisa.getElementsByTagName(tagName1)
	for prodBibliograficaX in prodBibliografica:
		artigosPublicados = prodBibliograficaX.getElementsByTagName(tagName2)
		if len(artigosPublicados) <> 0:
			for artigosPublicadosX in artigosPublicados:
				artigoPublicados = artigosPublicadosX.getElementsByTagName(tagName3)
		else:
			artigoPublicados = [];
	return artigoPublicados

def funcExamningBoardMestrado(fileXML):
	tagName1 = "DADOS-COMPLEMENTARES"
	tagName2 = "PARTICIPACAO-EM-BANCA-TRABALHOS-CONCLUSAO"
	tagName3 = "PARTICIPACAO-EM-BANCA-DE-MESTRADO"
	cvLattesAnalisa = funcCvLattesAnalisa(fileXML)
	prodBibliografica = cvLattesAnalisa.getElementsByTagName(tagName1)
	for prodBibliograficaX in prodBibliografica:
		artigosPublicados = prodBibliograficaX.getElementsByTagName(tagName2)
		if len(artigosPublicados) <> 0:
			for artigosPublicadosX in artigosPublicados:
				BancasRealizadas = artigosPublicadosX.getElementsByTagName(tagName3)
		else:
			BancasRealizadas = [];
	return BancasRealizadas	

def funcExamningBoardDoutorado(fileXML):
	tagName1 = "DADOS-COMPLEMENTARES"
	tagName2 = "PARTICIPACAO-EM-BANCA-TRABALHOS-CONCLUSAO"
	tagName3 = "PARTICIPACAO-EM-BANCA-DE-DOUTORADO"
	cvLattesAnalisa = funcCvLattesAnalisa(fileXML)
	prodBibliografica = cvLattesAnalisa.getElementsByTagName(tagName1)
	for prodBibliograficaX in prodBibliografica:
		artigosPublicados = prodBibliograficaX.getElementsByTagName(tagName2)
		if len(artigosPublicados) <> 0:
			for artigosPublicadosX in artigosPublicados:
				BancasRealizadas = artigosPublicadosX.getElementsByTagName(tagName3)
		else:
			BancasRealizadas = [];
	return BancasRealizadas	

def funcCheckFile(fileToCheck):
	fileCheck = 0;
	if os.path.isfile(fileToCheck):
		fileCheck = 1;
	return fileCheck

def imprimirAdd(arquivoPapers,imprimirMsg):
	msg = []
	del msg[:]

	dataInFile = funcGetDataInFile(arquivoPapers);
	fLog = open(arquivoPapers, "w");
	fLog.write(dataInFile);
	fLog.write("\n");
	
	fLog.write(str(imprimirMsg));
	
	#for msgImprimir in imprimirMsg:
	#	msg.append(msgImprimir)
	#	fLog.write(str(msg));
	#	del msg[:]
	fLog.close();
	return 0

def funcGetDataInFile(artigobase):
	fLogData = codecs.open(artigobase, "r")
	dataInFile = fLogData.read()
	fLogData.close()
	return dataInFile

def imprimirBase(programId):
	pathArtigos = "C:\\PesquisaLattes\\Matrizes\\"
	arquivo = "artigo"
	tipo = "csv"
	checkData = 1
	artigobase = pathArtigos + arquivo + "." + tipo
	
	arquivoUsado = pathArtigos + arquivo + "_" + programId + "." + tipo
	
	fileCheck = funcCheckFile(arquivoUsado)
	
	if fileCheck <> checkData:
		dataInFile = funcGetDataInFile(artigobase);
		dataLog = open(arquivoUsado, "w");
		dataLog.write(dataInFile);
		dataLog.close();

	return arquivoUsado
	
def imprimirBoard(programId):
	pathArtigos = "C:\\PesquisaLattes\\Matrizes\\"
	arquivo = "board"
	tipo = "csv"
	checkData = 1
	artigobase = pathArtigos + arquivo + "." + tipo
	
	arquivoUsado = pathArtigos + arquivo + "_" + programId + "." + tipo
	
	fileCheck = funcCheckFile(arquivoUsado)
	
	if fileCheck <> checkData:
		dataInFile = funcGetDataInFile(artigobase);
		dataLog = open(arquivoUsado, "w");
		dataLog.write(dataInFile);
		dataLog.close();

	return arquivoUsado

def funcProdBibliograficaCoautor(fileXML,fileLog,runYear,listDataTeacher,listCoauthor,contTeacher,matrizAdjacente,qtdPaperRerturn,tituloUnico,programId):
	# fileXML							# Arquivo XML
	# fileLog							# Arquivo de log usado na execução
	# listDataTeacher					# Dados do Docente
	# listCoauthor						# Lista de Coautores Docentes
	# contTeacher						# Contador da Linha
	# matrizAdjacente					# Matriz de Adjacencia
	tagData = []
	attributeData = []
	returnMatrizAdjacente = []
	matriz = []
	uniqueTitle = []
	uniqueID = []
	imprimirMsg = []
	
	del tagData[:]
	del attributeData[:]
	del returnMatrizAdjacente[:]
	del matriz[:]
	del uniqueTitle[:]
	del uniqueID[:]
	del imprimirMsg[:]
	
	arquivoPapers = imprimirBase(programId)
	
	tagData.append("DADOS-BASICOS-DO-ARTIGO")						# 0	- Basic Data
	tagData.append("DETALHAMENTO-DO-ARTIGO")						# 1	- Paper Detail
	tagData.append("AUTORES")										# 2	- Authors
	
	attributeData.append("TITULO-DO-ARTIGO")						# 0	- Title
	attributeData.append("ANO-DO-ARTIGO")							# 1	- Year
	attributeData.append("DOI")										# 2	- DOI
	attributeData.append("TITULO-DO-PERIODICO-OU-REVISTA")			# 3	- Newspaper
	attributeData.append("ISSN")									# 4	- ISSN
	
	countAuthor = 0
	paperWrite = 0
	paperCoauthorship = 0
	somaCoauthorship = 0
	
	totalAutores = 0
	totalCoautores = 0
	qtStartPapers = len(qtdPaperRerturn)
	
	paperPublish = funcProdBibliografica(fileXML)	
	for paperPublished in paperPublish:
		paperCoauthorshipCheck = 0
		qtdCoauthorship = 0
		
		dadosBasicosArtigo = paperPublished.getElementsByTagName(tagData[0])[0];
		anoPublicacaoDadoArtigo = dadosBasicosArtigo.getAttribute(attributeData[1])
		
		if anoPublicacaoDadoArtigo == runYear:

			msgLog = "				> TITULO"
			modLog.funcAddLog(fileLog,msgLog)
			
			detailPaper = paperPublished.getElementsByTagName(tagData[1])[0]
			coauthorPaper = paperPublished.getElementsByTagName(tagData[2])
			
			paperTitle = dadosBasicosArtigo.getAttribute(attributeData[0])
			paperDOI = dadosBasicosArtigo.getAttribute(attributeData[2])
			newspaperName = detailPaper.getAttribute(attributeData[3])
			newspaperISSN = detailPaper.getAttribute(attributeData[4])
			
			#imprimirMsg.append(programId)
			#imprimirMsg.append(paperTitle)
			#imprimirMsg.append(newspaperName)
			#imprimirMsg.append(newspaperISSN)
			#imprimirMsg.append(anoPublicacaoDadoArtigo)
			#imprimirAdd(arquivoPapers,imprimirMsg)
			
			returnMatrizAdjacente = funcCoautorProdBibligrafico(coauthorPaper,listDataTeacher,listCoauthor,contTeacher,matrizAdjacente,fileLog,uniqueID)
			
			matrizAdjacente = returnMatrizAdjacente[0]
			paperCoauthorshipCheck = returnMatrizAdjacente[1]
			qtdCoauthorship = returnMatrizAdjacente[2]
			uniqueID = returnMatrizAdjacente[3]
			
			paperWrite = paperWrite + 1
			#uniqueTitle = modValida.funcInsertUniqueTitle(paperTitle,newspaperName,newspaperISSN,qtdPaperRerturn)
			#tituloUnico = modValida.funcInsertUniqueTitle(paperTitle,newspaperName,newspaperISSN,qtdPaperRerturn)
			qtEndPaper = len(uniqueTitle)
			
			#########################
			#para testes
			#print(paperTitle)
			#########################
			
			if paperCoauthorshipCheck == 1:
				paperCoauthorship = paperCoauthorship + 1
			
			if qtEndPaper > qtStartPapers:
				totalAutores = totalAutores + qtdCoauthorship
				totalCoautores = totalCoautores + paperCoauthorshipCheck
			
			qtStartPapers = qtEndPaper
			
			"""
			#Use to validade data
			print("Quantidade Autores: " + str(qtdCoauthorship))
			print("Quantidade Coautoria: " + str(paperCoauthorshipCheck))
			print("-----")
			print("Soma de Artigos: " + str(qtStartPapers))			#####
			print("Soma Autores: " + str(totalAutores))				#####
			print("Soma Coautoria: " + str(totalCoautores))			#####
			"""
		somaCoauthorship = somaCoauthorship + qtdCoauthorship
	print("	> Total de artigos: " + str(paperWrite))
	return matrizAdjacente,qtStartPapers,totalAutores,totalCoautores,uniqueTitle,uniqueID,tituloUnico

def funcCoautorProdBibligrafico(coautoresArtigo,listDataTeacher,listCoauthor,contTeacher,matrizAdjacente,logFileRun,uniqueID):
	#listUniqueQuoteAuthor = []
	attributeData = []
	del attributeData[:]

	attributeData.append("NOME-COMPLETO-DO-AUTOR")						# 0 - Author
	attributeData.append("NRO-ID-CNPQ")									# 1 - Lattes ID
	attributeData.append("NOME-PARA-CITACAO")							# 2 - Quote
	msgLog = ""
	paperCoauthorship = 0
	checkData = 1
	checkCouthorship = 0
	qtdCoauthorship = 0
	
	for coautoresArtigoX in coautoresArtigo:
		checkCouthorship = 0
		qtdCoauthorship = qtdCoauthorship + 1
		validateName = "x"
		validateID = "x"
		validateCitation = "x"
		validateCheck = -1
		updateMatriz = -1
		
		paperNameCoauthor = coautoresArtigoX.getAttribute(attributeData[0]).upper()
		paperLattesIDCoauthor = coautoresArtigoX.getAttribute(attributeData[1])
		paperCitationCoauthor = coautoresArtigoX.getAttribute(attributeData[2])
		#listUniqueQuoteAuthor.append(paperCitationCoauthor)
		
		### buscar coluna ###
		validateName = modValida.funcValidaDados(paperNameCoauthor,listCoauthor)
		validateID = modValida.funcValidaDados(paperLattesIDCoauthor,listCoauthor)
		validateCitation = modValida.funcValidaCitacao(paperCitationCoauthor,listCoauthor)
		
		msgLog = "					>> IDENFICADO POR "
		
		if str(validateCheck) <> str(validateName):
			#identificas nome
			msgLog = msgLog + "NOME"
			updateMatriz = validateName
			uniqueID = modValida.funcIdentifyAuthorID(listCoauthor,validateName,uniqueID)
		elif str(validateCheck) <> str(validateID):
			#identifica id
			msgLog = msgLog + "ID"
			updateMatriz = validateID
			uniqueID = modValida.funcIdentifyAuthorID(listCoauthor,validateName,uniqueID)
		elif str(validateCheck) <> str(validateCitation):
			#identifica citacao
			msgLog = msgLog + "CITACAO"
			updateMatriz = validateCitation
			uniqueID = modValida.funcIdentifyAuthorID(listCoauthor,validateName,uniqueID)
			#print(msgLog)
			
		# change matriz if have coauthorship
		if updateMatriz > -1:
			if contTeacher <> updateMatriz:
				checkCouthorship = 1
				modLog.funcAddLog(logFileRun,msgLog)
				msgLog = "					>> Linha: " + str(contTeacher) + " | Coluna: " + str(updateMatriz)
				modLog.funcAddLog(logFileRun,msgLog)
				#print(msgLog)
				
				matrizAdjacente[contTeacher][updateMatriz] = 1
				matrizAdjacente[updateMatriz][contTeacher] = 1
				
				msgLog = "					>> Matriz atualizada"
				modLog.funcAddLog(logFileRun,msgLog)
				msgLog = " "
				modLog.funcAddLog(logFileRun,msgLog)
	
		if checkData == checkCouthorship:
			paperCoauthorship = 1
	if paperCoauthorship < 1:
		msgLog = "					>> Sem Coautoria"
		modLog.funcAddLog(logFileRun,msgLog)
		
	return matrizAdjacente,paperCoauthorship,qtdCoauthorship,uniqueID
	
def funcExaminingBoard(fileXML,fileLog,runYear,listDataTeacher,listCoauthor,contTeacher,matrizAdjacente,qtdPaperRerturn,tituloUnico,programId):
	# fileXML							# Arquivo XML
	# fileLog							# Arquivo de log usado na execução
	# listDataTeacher					# Dados do Docente
	# listCoauthor						# Lista de Coautores Docentes
	# contTeacher						# Contador da Linha
	# matrizAdjacente					# Matriz de Adjacencia
	tagData = []
	attributeData = []
	returnMatrizAdjacente = []
	matriz = []
	uniqueTitle = []
	uniqueID = []
	imprimirMsg = []
	
	del tagData[:]
	del attributeData[:]
	del returnMatrizAdjacente[:]
	del matriz[:]
	del uniqueTitle[:]
	del uniqueID[:]
	del imprimirMsg[:]

	arquivoBoard = imprimirBoard(programId)

	tagData.append("DADOS-BASICOS-DA-PARTICIPACAO-EM-BANCA-DE-MESTRADO")	# 0	- Dados de Mestrado
	tagData.append("DADOS-BASICOS-DA-PARTICIPACAO-EM-BANCA-DE-DOUTORADO")	# 1	- Dados de Doutorado	
	tagData.append("PARTICIPANTE-BANCA")									# 2	- Doutores
	
	
	attributeData.append("NATUREZA")										# 0	- Mestrado / Doutorado			# Tag[0-1]
	attributeData.append("ANO")												# 1	- Ano							# Tag[0-1]
	attributeData.append("TIPO")											# 2	- Acadêmico / Profissional		# Tag[0]
	attributeData.append("NOME-COMPLETO-DO-PARTICIPANTE-DA-BANCA")			# 3	- Nome Doutor					# Tag[2]
	attributeData.append("NOME-PARA-CITACAO-DO-PARTICIPANTE-DA-BANCA")		# 4	- Citação Doutor				# Tag[2]
	attributeData.append("NRO-ID-CNPQ")										# 5	- Lattes ID						# Tag[2]
	
	countAuthor = 0
	paperWrite = 0
	paperCoauthorship = 0
	somaCoauthorship = 0
	
	totalAutores = 0
	totalCoautores = 0
	qtStartPapers = len(qtdPaperRerturn)
	
	boardMestrados = funcExamningBoardMestrado(fileXML)
	boardDoutorados = funcExamningBoardDoutorado(fileXML)

	# Observação Mestrado
	for boardMestrado in boardMestrados:
		paperCoauthorshipCheck = 0
		qtdCoauthorship = 0
		
		dadosBasicos = boardMestrado.getElementsByTagName(tagData[0])[0];		# Mestrado
		#dadosBasicos = boardMestrado.getElementsByTagName(tagData[1])[0];		# Doutorado
		anoPublicacaoDadoArtigo = dadosBasicos.getAttribute(attributeData[1])	# Ano
		
		if anoPublicacaoDadoArtigo == runYear:			
			msgLog = "				> TITULO"
			modLog.funcAddLog(fileLog,msgLog)
			
			detailPaper = boardMestrado.getElementsByTagName(tagData[0])[0]
			boardDoctors = boardMestrado.getElementsByTagName(tagData[2])
			
			boardNature = dadosBasicos.getAttribute(attributeData[0])
			boardTipo = dadosBasicos.getAttribute(attributeData[2])				# apenas para mestrado atualmente
			
			#imprimirMsg.append(programId)
			#imprimirMsg.append(paperTitle)
			#imprimirMsg.append(newspaperName)
			#imprimirMsg.append(newspaperISSN)
			#imprimirMsg.append(anoPublicacaoDadoArtigo)
			#arquivoPapers = imprimirAdd(arquivoPapers,imprimirMsg)
			
			returnMatrizAdjacente = funcBancaDoutores(boardDoctors,listDataTeacher,listCoauthor,contTeacher,matrizAdjacente,fileLog,uniqueID)
			
			matrizAdjacente = returnMatrizAdjacente[0]
			paperCoauthorshipCheck = returnMatrizAdjacente[1]
			qtdCoauthorship = returnMatrizAdjacente[2]
			uniqueID = returnMatrizAdjacente[3]
			
			#paperWrite = paperWrite + 1
			#uniqueTitle = modValida.funcInsertUniqueTitle(paperTitle,newspaperName,newspaperISSN,qtdPaperRerturn)
			#tituloUnico = modValida.funcInsertUniqueTitle(paperTitle,newspaperName,newspaperISSN,qtdPaperRerturn)
			#qtEndPaper = len(uniqueTitle)
			
			#########################
			#para testes
			#print(paperTitle)
			#########################
			
			#if paperCoauthorshipCheck == 1:
			#	paperCoauthorship = paperCoauthorship + 1
			
			#if qtEndPaper > qtStartPapers:
			#	totalAutores = totalAutores + qtdCoauthorship
			#	totalCoautores = totalCoautores + paperCoauthorshipCheck
			
			#qtStartPapers = qtEndPaper
			
			
			#Use to validade data
			#print("Quantidade Autores: " + str(qtdCoauthorship))
			#print("Quantidade Coautoria: " + str(paperCoauthorshipCheck))
			#print("-----")
			#print("Soma de Artigos: " + str(qtStartPapers))			#####
			#print("Soma Autores: " + str(totalAutores))				#####
			#print("Soma Coautoria: " + str(totalCoautores))			#####
			
		#somaCoauthorship = somaCoauthorship + qtdCoauthorship
	
	# Observação Doutorado
	for boardDoutorado in boardDoutorados:
		paperCoauthorshipCheck = 0
		qtdCoauthorship = 0
		
		dadosBasicos = boardDoutorado.getElementsByTagName(tagData[1])[0];		# Doutorado
		anoPublicacaoDadoArtigo = dadosBasicos.getAttribute(attributeData[1])	# Ano
		
		if anoPublicacaoDadoArtigo == runYear:
			msgLog = "				> TITULO"
			modLog.funcAddLog(fileLog,msgLog)
			
			detailPaper = boardDoutorado.getElementsByTagName(tagData[1])[0]
			boardDoctors = boardDoutorado.getElementsByTagName(tagData[2])
			
			boardNature = dadosBasicos.getAttribute(attributeData[0])
			#boardTipo = dadosBasicos.getAttribute(attributeData[2])				# apenas para mestrado atualmente
			
			#imprimirMsg.append(programId)
			#imprimirMsg.append(paperTitle)
			#imprimirMsg.append(newspaperName)
			#imprimirMsg.append(newspaperISSN)
			#imprimirMsg.append(anoPublicacaoDadoArtigo)
			#arquivoPapers = imprimirAdd(arquivoPapers,imprimirMsg)
			
			returnMatrizAdjacente = funcBancaDoutores(boardDoctors,listDataTeacher,listCoauthor,contTeacher,matrizAdjacente,fileLog,uniqueID)
			
			matrizAdjacente = returnMatrizAdjacente[0]
			paperCoauthorshipCheck = returnMatrizAdjacente[1]
			qtdCoauthorship = returnMatrizAdjacente[2]
			uniqueID = returnMatrizAdjacente[3]
			
			#paperWrite = paperWrite + 1
			#uniqueTitle = modValida.funcInsertUniqueTitle(paperTitle,newspaperName,newspaperISSN,qtdPaperRerturn)
			#tituloUnico = modValida.funcInsertUniqueTitle(paperTitle,newspaperName,newspaperISSN,qtdPaperRerturn)
			#qtEndPaper = len(uniqueTitle)
			
			#########################
			#para testes
			#print(paperTitle)
			#########################
			
			#if paperCoauthorshipCheck == 1:
			#	paperCoauthorship = paperCoauthorship + 1
			
			#if qtEndPaper > qtStartPapers:
			#	totalAutores = totalAutores + qtdCoauthorship
			#	totalCoautores = totalCoautores + paperCoauthorshipCheck
			
			#qtStartPapers = qtEndPaper
			
			
			#Use to validade data
			#print("Quantidade Autores: " + str(qtdCoauthorship))
			#print("Quantidade Coautoria: " + str(paperCoauthorshipCheck))
			#print("-----")
			#print("Soma de Artigos: " + str(qtStartPapers))			#####
			#print("Soma Autores: " + str(totalAutores))				#####
			#print("Soma Coautoria: " + str(totalCoautores))			#####
			
		#somaCoauthorship = somaCoauthorship + qtdCoauthorship
		
	#print("	> Total de artigos: " + str(paperWrite))
	return matrizAdjacente,qtStartPapers,totalAutores,totalCoautores,uniqueTitle,uniqueID,tituloUnico

def funcBancaDoutores(boardDoctors,listDataTeacher,listCoauthor,contTeacher,matrizAdjacente,logFileRun,uniqueID):
	#listUniqueQuoteAuthor = []
	attributeData = []
	del attributeData[:]

	attributeData.append("NOME-COMPLETO-DO-PARTICIPANTE-DA-BANCA")		# 3	- Nome Doutor	# Tag[2]
	attributeData.append("NRO-ID-CNPQ")									# 5	- Lattes ID		# Tag[2]
	attributeData.append("NOME-PARA-CITACAO-DO-PARTICIPANTE-DA-BANCA")	# 4	- Quote			# Tag[2]
	
	msgLog = ""
	paperCoauthorship = 0
	checkData = 1
	checkCouthorship = 0
	qtdCoauthorship = 0
	
	for boardDoctorsX in boardDoctors:
		checkCouthorship = 0
		qtdCoauthorship = qtdCoauthorship + 1
		validateName = "x"
		validateID = "x"
		validateCitation = "x"
		validateCheck = -1
		updateMatriz = -1
		
		nameDoctor = boardDoctorsX.getAttribute(attributeData[0]).upper()
		lattesIDDoctor = boardDoctorsX.getAttribute(attributeData[1])
		citationDoctor = boardDoctorsX.getAttribute(attributeData[2])
		#listUniqueQuoteAuthor.append(paperCitationCoauthor)
		
		### buscar coluna ###
		validateName = modValida.funcValidaDados(nameDoctor,listCoauthor)
		validateID = modValida.funcValidaDados(lattesIDDoctor,listCoauthor)
		validateCitation = modValida.funcValidaCitacao(citationDoctor,listCoauthor)
		
		msgLog = "					>> IDENFICADO POR "
		
		if str(validateCheck) <> str(validateName):
			#identificas nome
			msgLog = msgLog + "NOME"
			updateMatriz = validateName
			uniqueID = modValida.funcIdentifyAuthorID(listCoauthor,validateName,uniqueID)
		elif str(validateCheck) <> str(validateID):
			#identifica id
			msgLog = msgLog + "ID"
			updateMatriz = validateID
			uniqueID = modValida.funcIdentifyAuthorID(listCoauthor,validateName,uniqueID)
		elif str(validateCheck) <> str(validateCitation):
			#identifica citacao
			msgLog = msgLog + "CITACAO"
			updateMatriz = validateCitation
			uniqueID = modValida.funcIdentifyAuthorID(listCoauthor,validateName,uniqueID)
			#print(msgLog)
			
		# change matriz if have coauthorship
		if updateMatriz > -1:
			if contTeacher <> updateMatriz:
				checkCouthorship = 1
				modLog.funcAddLog(logFileRun,msgLog)
				msgLog = "					>> Linha: " + str(contTeacher) + " | Coluna: " + str(updateMatriz)
				modLog.funcAddLog(logFileRun,msgLog)
				#print(msgLog)
				
				matrizAdjacente[contTeacher][updateMatriz] = 1
				matrizAdjacente[updateMatriz][contTeacher] = 1
				
				msgLog = "					>> Matriz atualizada"
				modLog.funcAddLog(logFileRun,msgLog)
				msgLog = " "
				modLog.funcAddLog(logFileRun,msgLog)
	
		if checkData == checkCouthorship:
			paperCoauthorship = 1
	if paperCoauthorship < 1:
		msgLog = "					>> Sem Coautoria"
		modLog.funcAddLog(logFileRun,msgLog)
	
	return matrizAdjacente,paperCoauthorship,qtdCoauthorship,uniqueID