# -*- coding: utf-8 -*-
"""
############################################################
# @Description: 
#
#
# @Author: Solon Macedonia Soares
# @Email: soares.solonm@gmail.com
# 
# Version: 1.0
# Data release: mm/dd/yyyy
############################################################
"""
########################## IMPORTS #########################
# Oficial modules
import sys
import os
import zipfile
import csv
# Personal modules
sys.path.append('C:\\PesquisaLattes\\Libs\\')
import supportListPPG as modPPG								# Work with data files to run program
import supportLog as modLog									# File Logs Functions

######################### FUNCTIONS #########################

###### WORK FILES
# Verify exist file
def funcCheckFile(fileToCheck):
	fileCheck = 0;
	if os.path.isfile(fileToCheck):
		fileCheck = 1;
	return fileCheck

def funcCheckFileMatrizPPG(fileCSV):
	checkData = 1
	msgLog = "		> Arquivo: " + fileCSV
	
	existFile = funcCheckFile(fileCSV);
	if existFile == checkData:
		os.remove(fileCSV);
		msgLog = msgLog + " > atualizado"
	else:
		msgLog = msgLog + " > criado";
	
	return msgLog

def funcDataPPG(fileCheck,fileLog):
	msgLog = "Arquivo encontrado: " + fileCheck
	checkData = 1
	existFile = funcCheckFile(fileCheck)
	
	if existFile <> checkData:
		msgLog = "Problema com o arquivo: " + fileCheck
		modLog.funcAddLog(fileLog,msgLog)
		print(msgLog)
		modLog.funcFinishLogFile(fileLog)
	else:
		modLog.funcAddLog(fileLog,msgLog)
	
	return 0

def funcListDataPPG(fileData,programId):
	listaData = []
	with open(fileData) as readFilePPG:
		for listCheckPPGs in readFilePPG:
			dataLine  = listCheckPPGs.rstrip().rsplit(";");
			if dataLine[0] == programId:
				listaData = dataLine;
	return listaData
	
# list PPGs to run program
def funcListPPG(filePPG,fileListPPG,fileLog,fileListId,pathXML,pathZip):
	checkData = 1
	checkPPG = ""
	listPPG = []
	listPPGRun = []
	listIds = []
	del listPPG [:]
	del listPPGRun [:]
	
	with open(filePPG) as readFilePPG:
		for listCheckPPGs in readFilePPG:
			programId = listCheckPPGs.rstrip()
			listPPG.append(programId)

	msgLog = "Programa(s) solicitado(s): " + str(len(listPPG))
	modLog.funcAddLog(fileLog,msgLog)
	print(msgLog)
	
	if len(listPPG) < checkData:
		funcNoPPGs(fileLog)
	
	programId = ""
	for programId in listPPG:
		del listIds [:]
		listIds = modPPG.funcListIdsPPG(fileListId,programId)

		msgLog = "> PPG: " + programId
		modLog.funcAddLog(fileLog,msgLog)		
		
		msgLog = "	> Lattes Ids: " + str(len(listIds))
		modLog.funcAddLog(fileLog,msgLog)

		checkXML = funcListXML(programId,listIds,pathXML,pathZip,fileLog) 
		
		if checkXML == checkData:
			#verfica dados do programa
			checkPPG = modPPG.funcCheckDataPPG(fileListPPG,programId)
			if checkPPG == 1:
				listPPGRun.append(programId)
			else:
				msgLog = "\n=> Programa " + programId + " faltam dados para observação."
				modLog.funcAddLog(fileLog,msgLog)
				print(msgLog)
		
	if len(listPPGRun) < checkData:
		funcNoPPGs(fileLog)
		
	return listPPGRun
	
def funcNoPPGs(fileLog):
	msgLog = "\n=> Sem programas para observar, fim do programa."
	modLog.funcAddLog(fileLog,msgLog)
	print(msgLog)
	modLog.funcFinishLogFile(fileLog)
	
	return 0

# check XML file by PPG
def funcListXML(programId,listIds,pathXML,pathZip,fileLog):
	returnData = 1
	checkData = 1
	for lattesId in listIds:
		msgLog = "		> " + lattesId + " => "
		fileXML = pathXML + lattesId + ".xml"
		
		existFile = funcCheckFile(fileXML)
		if existFile == checkData:
				msgLog = msgLog + "XML existe"
		else:			
			fileZip = pathZip + lattesId + ".zip"
			
			existFile = funcCheckFile(fileZip)
			if existFile == checkData:
				funcUnzipCVLattes(pathZip,pathXML,lattesId)
				
				existFile = funcCheckFile(fileXML)
				if existFile == checkData:
					msgLog = msgLog + "XML descompactado"
				else:
					msgLog = msgLog + "XML indisponivel"
					returnData = 0
			else:
				msgLog = msgLog + "ZIP indisponivel"
				returnData = 0
							
		modLog.funcAddLog(fileLog,msgLog);
		
	if returnData == 0:
		print("\n=> Para o programa " + programId + " falta(m) curriculo(s), verifique log")
	
	return returnData
"""
# control XML file
def funcUnzipXML(lattesXML,pathZip,pathXML):
	checkData = 1;											# TRUE
	fileZip = pathZip + lattesXML + ".zip";
	existFile = funcCheckFile(fileZip);
	
	if existFile == checkData:
		msgLog = "		> XML " + lattesXML + " unziped"
		msgLog = msgLog + funcUnzipCVLattes(pathZip,pathXML,lattesXML);
	else:
		msgLog = "		> ZIP " + lattesXML + " file not exist"
		
	return msgLog
"""
# Unzip XML file
def funcUnzipCVLattes(pathIn,pathOut,lattesID):
	checkData = 1;
	fileToUnzip = pathIn + lattesID + ".zip";

	fantasy_zip = zipfile.ZipFile(fileToUnzip);
	fantasy_zip.extractall(pathOut);
	fantasy_zip.close();
	
	checkUnzip = pathOut + "curriculo.xml";
	existFile = funcCheckFile(checkUnzip);
	
	if existFile == checkData:
		fileXML = pathOut + lattesID + ".xml";
		existFile = funcCheckFile(fileXML);
		if existFile == checkData:
			os.remove(fileXML);
			os.rename(checkUnzip,fileXML);
		else:
			os.rename(checkUnzip,fileXML);	
	return 0
"""
def funcListLattesId(filePPG):
	listIds = [];
	with open(filePPG) as readFilePPG:		
		for listCheckXML in readFilePPG:
			lattesXML = listCheckXML.rstrip();
			listIds.append(lattesXML);
	return listIds
"""
def funcExportDataRunCSV(pathMatriz,listRunDataCSV,logFile):
	counter = 0
	fileRunCSV = pathMatriz + "dados.ini"
	
	msgLog = ">>> Criando lista para rodar iGraph."
	modLog.funcAddLog(logFile,msgLog)
	print(msgLog)
	
	if funcCheckFile(fileRunCSV) == 1:
		os.remove(fileRunCSV)
		msgLog = "	> Arquivo dados.ini atualizado com sucesso"
	else:
		msgLog = "	> Arquivo dados.ini exportado com sucesso."
	
	dataExportCSV = open(fileRunCSV, "a+")
	for line in listRunDataCSV:
		counter = counter + 1
		valueCount = str(counter).rjust(3)
		valueCount = valueCount.replace(" ","0")
		lineWrite = valueCount + "=.\\" + "\\" + line + "\n"
		dataExportCSV.write(lineWrite)
	dataExportCSV.close()
	
	modLog.funcAddLog(logFile,msgLog)
	print(msgLog)	
	
	return 0